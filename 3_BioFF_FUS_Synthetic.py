#!/usr/bin/env python
# coding=utf-8
###################################################################################
import numpy as np
import os
import os.path
import matplotlib.pyplot as plt
import matplotlib
import time
import pymbar as mbar
from pathos.multiprocessing import ProcessingPool as Pool
import shutil
#from multiprocessing import cpu_count
#from multiprocessing.pool import Pool
from joblib import Parallel, delayed
import pickle

## Pieces of code
# Import the constants
from BioFF_FUS import constants as c
from BioFF_FUS import Bookeeping as bk
from BioFF_FUS import MBAR_and_inverseSampling as mbis
from BioFF_FUS import Observables as obs

from BioFF_FUS import GMX_functions as gmxf
from BioFF_FUS import Synthetic_sim as ssim
from BioFF_FUS import BioFF_von_Mises as bioff
from BioFF_FUS import BioFF as Bio

#pandas
import pandas as pd

# For running BioFF
import scipy.stats
import itertools

import asyncio
import MDAnalysis as mda

import asyncmd
from asyncmd import gromacs as asyncgmx
from asyncmd import trajectory as asynctraj



#%config Application.log_level='WORKAROUND'  # need this fail for the next one to work...
#loglevel = "WARN"
#loglevel = "DEBUG"
#%config Application.log_level=loglevel

#import logging
#l = logging.getLogger("asyncmd")
#l.setLevel(loglevel)


## FUNCTIONS
max_iterations = 50
kish_tot = np.ones((max_iterations,50))*-1
def Num():
    save_dict, df_rerun, df_sim = Bio.open_pickle()
    save_dict['SKL_val'].append([])
    save_dict['chi2_val'].append([])
    save_dict['L_val'].append([])
    save_dict['obj_val'].append([])
    save_dict['kish_tot'].append([])
    save_dict['w'].append([])
    save_dict['all_u'].append([])
    Bio.close_pickle(save_dict, df_rerun, df_sim)
    
class Kish():    
    n=0
kish=Kish()

def kish_num_update(kish):
    kish.n += 1
    return 

def objective_function(ff_param, ff_param_old, w0, yTilde, YTilde,  all_u_refs, theta, n_iterations, n_threshold = 300, k_threshold=10):
    """
    Objective function for BioFF optimization.
    
    Args: 
        alpha (float): The current scaling parameter of the LJ pair-pair interaction. It should be replaced by dictionary for multiple parameters. 
        w0: (array): The reference weights and the reference alpha-value
        all_u (array): All energies for the current value of alpha
        yTilde (array): The calculated observables for all members in the ensemble divided by the total error
        YTilde (float): Experimental average value of the observable 
        theta (float): Trading off experimental and theoretical information (BioEn)
        n_threshold (int): Kish sample size parameter threshold 
        k_threshold (float): Parameter set to avoid leaving sample space by setting threshold on Kish' sample size (BioFF paper)
        
    Return:

    """
    ##Pickle the time
    start_time2 = time.time()   
    # add calculation of all_u for current alpha value 
    print("Let's try the value", ff_param)
    all_u = recalculate_energy(ff_param, ff_param_old)  # Here
    recal_energy1.append(float(time.time()-start_time2))
    Bio.save_recal_ener_time(float(time.time()-start_time2))   # look here
    energy1.append(all_u)
    Bio.save_all_u(all_u)
    

    print("length_all_u",len(all_u))
    print("mean_all_u", np.mean(all_u))
    
    start_time3 = time.time()
    w = bioff.reweighting(all_u, all_u_refs, w0)
    reweight_time1.append(float(time.time()-start_time3))
    Bio.save_reweighting_time(float(time.time()-start_time3))   # look here
    Bio.save_w(w)                                              
    weight1.append(w)
    
    print("w", w)
    print("w-shape", w.shape)
    L_val, SKL_val, chi2_val = bioff.neg_log_posterior(w, w0, yTilde, YTilde, theta)
    print("Lval", L_val)
    n = bioff.effective_sample_size(w)
    
    #Include to keep track of the kish sampling size
    Bio.save_SKL_val(SKL_val)                                   # look here
    Bio.save_L_val(L_val)                                       # look here
    Bio.save_CHI_val(chi2_val)                                  # look here
    kish_num_update(kish)
    Bio.save_kish_sampling_size(n)                       # look here
    Bio.save_OBJ_val(L_val*bioff.f_threshold(n, n_threshold, k_threshold))
    print("kish sampling size in every iteration", kish_tot)
    print("n", n)
    print(L_val*bioff.f_threshold(n, n_threshold, k_threshold))
    return L_val*bioff.f_threshold(n, n_threshold, k_threshold)

def recalculate_energy(ff_parameter_value_new, ff_param_old):
    """
    Recalculates the energy with the new ff_parameter for the configurations obtained with the ff_param_old
    -----------------------------------------
    Parameters
    ff_parameter_value_new (float) (list)
    ff_param_old (float) (list)
    -----------------------------------------
    Return
    new energies
    """
    # create an empty numpy array to store the differences in energy
    x_new_ener=[]
    
    # Iterates over the old parameters to rerun the energies
    for ff_parameter in ff_param_old:
        ff_parameter_list = []
        ff_parameter_list.append(ff_parameter)
        ff_parameter_list.append(ff_parameter_value_new)
        print('The ff values are', ff_parameter_list)
        path=bk.path_to_energy_folder_rerun(ff_parameter_list[0], ff_parameter_list[1])    # Here
        mbis.rerun_energies(ff_param1=ff_parameter_list[0], ff_param2=ff_parameter_list[1], path, verbose=True)
        #file=bk.path_to_energy_file_rerun(ff_parameter_list[0], ff_parameter_list[1])
        file=path+'/'+c.ENERGY_FILE_NAME
        x_ener= mbis.load_energy_from_file(file)
        x_new_ener=np.append(x_new_ener,np.asarray(x_ener[1]))
    
    return x_new_ener

def recalculate_energy11(ff_parameter_value_new, ff_param_old):
    """
    Recalculates the energy with the new ff_parameter for the configurations obtained with the ff_param_old
    -----------------------------------------
    Parameters
    ff_parameter_value_new (float)
    ff_param_old (float)
    -----------------------------------------
    Return
    new energies
    """
    # create an empty numpy array to store the differences in energy
    x_new_ener=[]
    
    # Iterates over the old parameters to rerun the energies
    def iterate_ff_param(ff_parameter_num):
        ff_parameter_list = [ff_param_old[ff_parameter_num], ff_parameter_value_new]
        print ('The ff values are', ff_parameter_list)
        path=bk.path_to_energy_folder_rerun(ff_parameter_list[0], ff_parameter_list[1])
        mbis.rerun_energies(ff_param1=ff_parameter_list[0], ff_param2=ff_parameter_list[1],  verbose=True )
        return

    #p=Pool(10)
    #results=p.map(iterate_ff_param, range(len(ff_param_old)))
    #p.close()
    #p.join()

    results=Parallel(n_jobs=10, prefer="threads")(delayed(iterate_ff_param)(i) for i in range(len(ff_param_old)))

    for ff_parameter in ff_param_old:
        ff_parameter_list=[ff_parameter, ff_parameter_value_new]
        file=bk.path_to_energy_file_rerun(ff_parameter_list[0], ff_parameter_list[1])
        x_ener=  mbis.load_energy_from_file(file)
        x_new_ener=np.append(x_new_ener, np.asarray(x_ener[1]))
    return x_new_ener


##Define a function to prepare the rerun of energies and MBAR
def chNamesOutputFiles(wdir, name_job, XTC=c.XTC_NAME, TPR=c.TPR_NAME, ENER=c.ENER_EDR):
    os.chdir(wdir)
    os.system(" mv {}.part0001.xtc {}".format(name_job, XTC))
    os.system(" mv {}.tpr {}".format(name_job, TPR))
    os.system(" mv {}.part0001.edr {}".format(name_job, ENER))

#############################
# INITIALIZE THE PARAMETERS #
#############################

sbatch=c.SBATCH_MASTER

## Define alpha
#alpha value for initial (or reference) simulation from the user data
alpha_0 = [1.0] #args.alpha_0

#alpha value for synthetic experimental data
alpha_exp = [0.8] #args.alpha_exp #0.7 

##Define parameters for BioFF - run
theta = .01 #args.theta

print('----------------------------------------------------------------------------')
print('--------------------- Read data from synthetic experiment  -----------------')
print('----------------------------------------------------------------------------')

# Load the obsevable (radius of gyration) for the initial value of the force-field
path=bk.path_to_simulation_files(alpha_0)
rg_alpha_0 = obs.rad_gyration(path, xtc_unwrapped='md_pbcmol_centered.xtc', gro_unwrapped='md_pbcmol_centered.gro')
av_sim_Rg= obs.mean_Rg(np.asarray(rg_alpha_0))
print("The radius of gyration predicted by current ff is {}".format(av_sim_Rg))
# yTlide is an input for BioFF - see Eq. (10)
y = np.asarray(rg_alpha_0)
print(y.shape)
stderr=1
yTilde = y/stderr

print("mean yTilde = %3.2f" % obs.mean_Rg(yTilde))

print('----------------------------------------------------------------------------')
print('--------------------- Load energies for inital ff-value    -----------------')
print('----------------------------------------------------------------------------')
# Load the energy (Here only LJ)
path=bk.path_to_energy_file(alpha_0)
energy_alpha_0_ts=mbis.load_energy_from_file(path)
energy_alpha_0=energy_alpha_0_ts[1]
#
## all_u_refs is the input for BioFF
all_u_refs = np.asarray(energy_alpha_0)
print(len(all_u_refs))




# Load the obsevables (radius of gyration) for the experiment.
path=bk.path_to_simulation_files(alpha_exp)
rg_alpha_exp = obs.rad_gyration(path, xtc_unwrapped='md_pbcmol_centered.xtc', gro_unwrapped='md_pbcmol_centered.gro')

#rg_alpha_exp
ave_rg_alpha_exp = obs.mean_Rg(np.asarray(rg_alpha_exp))
print("The 'experimental' radius of gyration is {}".format(ave_rg_alpha_exp))
## We need the experimental average value YTilde - input for BioFF - see Eq. (10) - paper. 
Y = ave_rg_alpha_exp
stderr = 1
YTilde = Y/stderr
print("YTilde = %3.2f" % YTilde)


print('----------------------------------------------------------------------------')
print('--------------------- Initial weights (w0)  --------------------------------')
print('----------------------------------------------------------------------------')
#
N_w = y.shape[0]
w0 = np.ones(N_w)/N_w

print(N_w)
print(w0)

alpha_new = alpha_0 # Starts with the current value of the ff-
#list for all optimized alpha valuesa
alphas2Bopt = [alpha_new] 
alphas2Bopt


#############
# FIRST RUN #
#############

print('----------------------------------------------------------------------------')
print('---------------------  First run  ------------------------------------------')
print('----------------------------------------------------------------------------')
#
n_iterations=0
i = 0
Bio.save_ff_params(alpha_new)
Bio.save_w0(w0)
#Bio.save_all_u(all_u)
Bio.save_YTilde(YTilde)
Bio.save_all_u_refs(all_u_refs)
Bio.save_theta(theta)
Bio.save_yTilde(yTilde)
Num()
start_time = time.time()
#def optimize_objective_func(alpha_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, n_iterations):
res = scipy.optimize.minimize(objective_function, alpha_new, args=(alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, n_iterations, ), method="Nelder-Mead", tol=.1, options={'maxiter':20})
#    return res

#res = optimize_objective_func(alpha_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, n_iterations)
print('------', res, '------')
print("optimization time in seconds",time.time()-start_time)

Bio.save_kish_num_update(kish.n)                     ### HERE Serious modification needed
kish=Kish()    # set the inner iterater to 0 inside the objective function. to record the kish
Bio.save_optimization_time(float(time.time()-start_time))

### Append it to the list of observable
alpha_new = res['x'][0]
print(alpha_new)
alphas2Bopt.append(alpha_new)

#save_all['alphas'].append(alpha_new)
Bio.save_ff_params(alpha_new)

print('----------------------------------------------------------------------------')
print('--------------------- Starting main iteration-------------------------------')
print('--------------------- Starting main iteration-------------------------------')
print('----------------------------------------------------------------------------')

times=[]
n_iterations = 1
max_iterations = 50 #maximum number of BioFF iterations; at most to the length of alpha grid use for iteration
is_converged = False

start_time0 = time.time()

while is_converged == False or n_iterations <= max_iterations:
    print('----------------------------------------------------------------------------')
    print('--------------------- We will create a new  synthetic simulation ------------')
    print('----------------------alpha={}, iteration {}'.format(alpha_new, n_iterations))
    print('----------------------------------------------------------------------------')


    #Recalculate energy
    idx_ran_energ, ff_parameter_list=ssim.recalculate_energy_for_synthetic_sim(alpha_new)

    # Create a synthetic simulations
    ssim.create_synthetic_simulation(ff_parameter_list, idx_ran_energ)

    print(alphas2Bopt)


    ## @Rajani: added this part of the code to fix (just temp.)

    shutil.copy(str(c.ROOT_DIR+"input_files/md_pbcmol_centered.gro"), str(c.ROOT_DIR+'simulations_BioFF/'+"alpha_"+str(r'{:.4f}'.format(alpha_new))))
    ## ------ end of the modification ------

    rg_alpha_new_new = obs.rad_gyration(bk.path_to_simulation_files(ff_parameter_list[2]))

    y = np.append(y, np.asarray(rg_alpha_new_new))
    print("y.shape",y.shape)
    yTilde = y/stderr
    print("mean yTilde = %3.2f" % obs.mean_Rg(yTilde))



    print(alphas2Bopt)

    print('--------------------- Running LJ-Energy fot the new simulation -------------')
    print('----------------------------------------------------------------------------')

    gmxf.gmx_energy(group=str(13), gmx=c.GMX, ener_file='ener.edr', output=c.ENERGY_FILE_NAME, verbose=False, output_verbose='output.txt')

    print('--------------------- Append the observables from the new simulation-----------')
    print('----------------------------------------------------------------------------')


    ###Calculate the new weigths
    #if len(alphas2Bopt)>5:
    #    alphas2Bopt = alphas2Bopt[-5:]
    #    yTilde=yTilde[-5005:]
    #if len(alphas2Bopt) > 5:
    #    alphas2Bopt = alphas2Bopt[-5:]
    #    yTilde = yTilde[:2002].append(yTilde[].append(yTilde[:-2002]))
    N_w = [c.NFRAMES] * len(alphas2Bopt)
    u_kn_syn = mbis.apply_mbar_get_energies(alphas2Bopt, N_w)
    all_u_refs = u_kn_syn[0]
    weights_syn=mbis.mbar_get_weights(u_kn_syn, c.T, c.K_BOLTZMANN, N_w)
    w0=weights_syn[0]

    start_time5 = time.time()
    i=0
    Bio.save_ff_params(alpha_new)
    Bio.save_w0(w0)
    Bio.save_all_u(all_u)
    Bio.save_YTilde(YTilde)
    Bio.save_all_u_refs(all_u_refs)
    Bio.save_yTilde(yTilde)

    Num()
    res = scipy.optimize.minimize(objective_function, alpha_new, args=(alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, n_iterations, ), method="Nelder-Mead", tol=.1, options={'maxiter':10})
    #res = optimize_objective_func(alpha_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, n_iterations)
    Bio.save_kish_num_update(kish.n)                     ### HERE Serious modification needed
    Bio.save_optimization_time(float(time.time()-start_time5))
    kish=Kish()    # set the inner iterater to 0 inside the objective function. to record the kish
    

    print('------', res, '------')
    print("optimization time in seconds",time.time()-start_time)

    #Append time of this iteration
    times.append(time.time()-start_time)
    Bio.save_optimization_time(float(time.time()-start_time5))

    alpha_old = alpha_new
    alpha_new = res['x'][0]
    #     - Append new value of ff to the alphas2Bopt list
    alphas2Bopt.append(alpha_new)
    n_iterations+=1
    Bio.save_ff_params(alpha_new)

    #Convergence condition
    if bioff.convergence(alpha_old, alpha_new, threshold=0.005):
        is_converged = True
        break
print("The TOTAL optimization time in seconds:",time.time()-start_time0)
