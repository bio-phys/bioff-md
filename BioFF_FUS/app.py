import yaml
from . import BioFF_fresh as BioFF
from . import BioFF as Bio
from . import BioFF_von_Mises as BioFF_mises
from . import Bookeeping as bk
import asyncio
import pickle
import sys

class App:
    def __init__(self, yamlFile):
        self.yamlFile = yamlFile
        with open(yamlFile, 'r') as file:
            self.constant = yaml.safe_load(file)
        # use the function from the Bioff_von_mises for the default function
        Bio.write_f_func(BioFF_mises.f_threshold, self.constant)
        Bio.write_param_func(Bio.param_prior, self.constant)
        Bio.Constructor(self.constant)
            
    async def run(self):
        """
        The function run() starts the BioFF run. It neither takes any argument nor return any value.
        """
        alpha_new, alphas2Bopt, w0, yTilde, YTilde, all_u_refs, theta, y, stderr = BioFF.initialize_params(data = self.constant)
        alpha_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, y, stderr = BioFF.firstRun( 
            alpha_new, 
            alphas2Bopt, 
            w0, 
            yTilde, 
            YTilde, 
            all_u_refs, 
            theta, 
            y, 
            self.constant, 
            stderr
        ) 
        await BioFF.main(
            alpha_new, 
            alphas2Bopt, 
            w0, 
            yTilde, 
            YTilde,  
            all_u_refs, 
            theta, 
            y, 
            self.constant, 
            stderr
        )
        
    def thresholdFunction(self, func=None): 
        """
        The thresholdFunction() takes a Python function as an argument and passes it along to the BioFF code.      
        
        Default: 
            - No threshold function

        Arguments: 
            - func : It is a Python function which describes the threshold function and takes two **args and one **kwargs.
                     The arguments for this python function are as follows:
                        --  n  : effective sampling size 
                        --  n0 : lower threshold on sampling size 
                        --  k  : steep constant

        Return:
            None 
        """
        if func is not None:
            Bio.write_f_func(func, self.constant)
        return

    def parameterPrior(self, func=None):
        """
        The parameterPrior() takes a Python function as an argument and passes it along to the BioFF code. 
        
        Default: 
            - No Parameter Prior

        Arguments: 
            - func : It is a Python function which describes the parameter prior and takes a list of one-dimensional parameter values for constructiing the parameter-prior. 

        Returns: 
            None 
        """
        if func is not None:
            Bio.write_param_func(func, self.constant)
        return
    
    def observable(self, func=None):
        """
        The oberservable() takes a python function as an argument and passes it along to BioFF code. 
        The Python function created by user will be used during calculation observables from the trajectory files. 
        
        Arguments: 
            - func : Python function which calculates observable from trajectory files and return the values in a list.
                     The argument for this python function is : 
                        -- path (str) : path of the folder where the simulation files exist
        Returns: 
            - None
        """
        if func is not None:
            Bio.write_observable_func(func, self.constant)
        return
    
    def expObservable(self, data=None):
        """
        The expObservable() takes list of experimental quantities and uses it to calculate chi-square.

        Arguments: 
            - data (list) : list of experimental quantities.

        Returns:
            - None
        """
        if data is not None:
            Bio.write_expObservable(data, self.constant)
        return

    def scaleItp(self, func=None):
        """
        The scaleItp() takes a python function as an argument and passes it along to the BioFF code.

        Arguments: 
            - func : Python function to scale itp files during BioFF run.
                     The arguments of the func() are: 
                        -- path (str)     : path of the folder where the itp needs to be modified (or scaled).
                        -- ffParam (list) : List of force field parameters in the same order as mentioned in constant file.
        Returns: 
            - None 
        """
        if func is not None:
            Bio.write_function_for_scaling_itp(func, self.constant)
        return

    def update(self, ROOT_DIR=None, PROJECT=None, RUN_STATE=None, NO_OF_FF_PARAMETER=None, THETA=None,
               INITIAL_SIMULATIONS=None, PICKLE_FILE=None, ENERGY_FILE_NAME=None, XTC_NAME=None,
               TPR_NAME=None, DECIMAL_PLACES=None, RERUN_FOLDER=None, ENER_EDR=None, FILES_TO_RERUN=None, 
               GMX_MODULE=None, GMX=None, GMX_MPI=None, PYTHON_STR=None, INITIAL_GRO_FILE=None, INIAL_GRO_FILE_NOWATER=None, 
               T=None, K_BOLTZMANN=None, NFRAMES=None, SBATCH_MASTER=None, FF_PARAM=None):
        """
        Update() function updates the constant file if user provides a value for any specific **kwargs realated to constant file.

        Arguments:
            - ROOT_DIR (str)           : path to the root directory
            - PROJET (str)             : path to the project directory
            - RUN_STATE (str)          : 'RESTART' or 'NEW' state
            - NO_OF_FF_PARAMETER (int) : number of force field parameters used in the BioFF run. 
            - THETA (float)            : Confidence parameter. e.g. 0.01
            - INITIAL_SIMULATIONS (str): Path to the available simulation files at reference force field value.
            - PICKLE_FILE (str)        : Provide a name to the pickle file.
            - ENERGY_FILE_NAME (str)   : Provide a name for the energy files (*.xvg) as available in simulation folders.
            - XTC_NAME (str)           : Provide a name for the *.xtc files as available in simulation folders.
            - TPR_NAME (str)           : Provide a name for the *.tpr files as available in simulation folders.
            - DECIMAL_PLACES (str)     : Provide number of decimal places to be used for forcefield parameters in BioFF calculations.
            - RERUN_FOLDER (str)       : Path to the folder where rerun files are stored.
            - ENER_EDR (str)           : Provide a name for the *.edr files as available in simulation folders. 
            - FILES_TO_RERUN (str)     : Provide a path which contains input files for starting a rerun/simulations.
            - GMX_MODULE (str)         : Provide a gromacs module which you would like to use.
            - GMX (str)                : Provide the path of GROMACS executable.
            - GMX_MPI (str)            : Provide the path of MPI-based GROMACS executable.
            - PYTHON_STR (str)         : Provide the python executable name. e.g., 'python3'
            - INITIAL_GRO_FILE (str)   : Provide a path to a GRO file as a structure file (in GROMACS).
            - INIAL_GRO_FILE_NOWATER (str) : Provide a path to a GRO file a structure file without any solvent molecule. 
            - T (float)                : Temperature
            - K_BOLTZMANN (float)      : Boltzmann constant 
            - NFRAMES                  : Number of frames in the trajectory files.
            - SBATCH_MASTER            : Provide the path to the master sbatch slurm file.
            - FF_PARAM                 : Provide a list of force field parameters to be optimized by BioFF code. 
            """
        
        if ROOT_DIR is not None:
            self.constant['ROOT_DIR'] = ROOT_DIR
        if PROJECT is not None:
            self.constant['PROJECT'] = PROJECT
        if FF_PARAM is not None:
            self.constant['FF_PARAM'] = FF_PARAM
        if RUN_STATE is not None: 
            self.constant['RUN_STATE'] = RUN_STATE
        if NO_OF_FF_PARAMETER is not None: 
            self.constant['NO_OF_FF_PARAMETER'] = NO_OF_FF_PARAMETER
        if THETA is not None: 
            self.constant['THETA'] = THETA
        if INITIAL_SIMULATIONS is not None: 
            self.constant['INITIAL_SIMULATIONS'] = INITIAL_SIMULATIONS
        if PICKLE_FILE is not None: 
            self.constant['PICKLE_FILE'] = PICKLE_FILE
        if ENERGY_FILE_NAME is not None: 
            self.constant['ENERGY_FILE_NAME'] = ENERGY_FILE_NAME
        if XTC_NAME is not None: 
            self.constant['XTC_NAME'] = XTC_NAME
        if TPR_NAME is not None: 
            self.constant['TPR_NAME'] = TPR_NAME
        if DECIMAL_PLACES is not None: 
            self.constant['DECIMAL_PLACES'] = DECIMAL_PLACES
        if RERUN_FOLDER is not None: 
            self.constant['RERUN_FOLDER'] = RERUN_FOLDER
        if ENER_EDR is not None: 
            self.constant['ENER_EDR'] = ENER_EDR
        if FILES_TO_RERUN is not None: 
            self.constant['FILES_TO_RERUN'] = FILES_TO_RERUN
        if GMX_MODULE is not None: 
            self.constant['GMX_MODULE'] = GMX_MODULE
        if GMX is not None: 
            self.constant['GMX'] = GMX
        if GMX_MPI is not None: 
            self.constant['GMX_MPI'] = GMX_MPI
        if PYTHON_STR is not None: 
            self.constant['PYTHON_STR'] = PYTHON_STR
        if INITIAL_GRO_FILE is not None: 
            self.constant['INITIAL_GRO_FILE'] = INITIAL_GRO_FILE
        if INIAL_GRO_FILE_NOWATER is not None: 
            self.constant['INIAL_GRO_FILE_NOWATER'] = INIAL_GRO_FILE_NOWATER
        if T is not None: 
            self.constant['T'] = T
        if K_BOLTZMANN is not None: 
            self.constant['K_BOLTZMANN'] = K_BOLTZMANN
        if NFRAMES is not None: 
            self.constant['NFRAMES'] = NFRAMES
        if SBATCH_MASTER is not None: 
            self.constant['SBATCH_MASTER'] = SBATCH_MASTER
            
        Bio.write_f_func(BioFF_mises.f_threshold, self.constant)
        Bio.write_param_func(Bio.param_prior, self.constant)
        Bio.Constructor(self.constant)
        return 

    def params(self, output=False):
        """
        params() function shows/returns the parameters used in the BioFF run. If the user sets the output as 'False', the params() returns 
        """
        if output == True:
            return self.constant
            
        else:
            print(f"""ROOT_DIR               = {self.constant['ROOT_DIR']}\nPROJECT                = {self.constant['PROJECT']}\nFF_PARAM               = {self.constant['FF_PARAM']}\nRUN_STATE              = {self.constant['RUN_STATE']}\nNO_OF_FF_PARAMETER     = {self.constant['NO_OF_FF_PARAMETER']}\nTHETA                  = {self.constant['THETA']}\nINITIAL_SIMULATIONS    = {self.constant['INITIAL_SIMULATIONS']}\nPICKLE_FILE            = {self.constant['PICKLE_FILE']}\nENERGY_FILE_NAME       = {self.constant['ENERGY_FILE_NAME']}\nXTC_NAME               = {self.constant['XTC_NAME']}\nTPR_NAME               = {self.constant['TPR_NAME']}\nPREFIX                 = {self.constant['PREFIX']}\nDECIMAL_PLACES         = {self.constant['DECIMAL_PLACES']}\nRERUN_FOLDER           = {self.constant['RERUN_FOLDER']}\nPREFIX_RERUN           = {self.constant['PREFIX_RERUN']}\nSUFIX_RERUN            = {self.constant['SUFIX_RERUN']}\nENER_EDR               = {self.constant['ENER_EDR']}\nFILES_TO_RERUN         = {self.constant['FILES_TO_RERUN']}\nGMX_MODULE             = {self.constant['GMX_MODULE']}\nGMX                    = {self.constant['GMX']}\nGMX_MPI                = {self.constant['GMX_MPI']}\nPYTHON_STR             = {self.constant['PYTHON_STR']}\nINITIAL_GRO_FILE       = {self.constant['INITIAL_GRO_FILE']}\nINIAL_GRO_FILE_NOWATER = {self.constant['INIAL_GRO_FILE_NOWATER']}\nCHECKPOINT             = {self.constant['CHECKPOINT']}\nRERUN_ENE_RDF          = {self.constant['RERUN_ENE_RDF']}\nINI_SIM_RDF            = {self.constant['INI_SIM_RDF']}\nT                      = {self.constant['T']}\nK_BOLTZMANN            = {self.constant['K_BOLTZMANN']}\nNFRAMES                = {self.constant['NFRAMES']}\nSBATCH_MASTER          = {self.constant['SBATCH_MASTER']}\n""")

    def addSimulation(self, simDict:dict):
        """
        addSimulation() updates the simulation entries from the DataFrame in the pickle file.

        Input:
            - simDict  : dictionary containing information about the folder names and the forcefield parameters
                         e.g. simDict = {
                                'simFolder1' : [1.0, 1.1, 1.23],   # forcefield parameters 
                                'simFolder2' : [1.1, 1.1, 1.2]     # of the simulations
                              }
        
        Returns: 
            - None
        """
        #print(simDict)
        if type(simDict) != dict:
            print("The input 'simDict' is not a dictionary. Please follow the following format - ")
            print("e.g. simDict = {")
            print("       'folderName'  : [1.0, 1.1, 1.23],   # forcefield parameters")
            print("       'folderName2' : [1.1, 1.1, 1.2]     # of the simulations")
            print("     }")
            sys.exit()
        else: 
            size = len(simDict)
            if size == 0:
                print("Please provide atleast one entry!")
                sys.exit()
            else:
                listItem = [item for item in simDict]
                print('Accepting entries!!')
                                
        save_dict, df_rerun, df_sim = Bio.open_pickle(self.constant)
        if len(simDict) != 0: 
            for item in simDict: 
                #print('simDict', simDict[item])
                listProperfloatingPoints = bk.list2list(simDict[item], self.constant)
                df_sim.loc[item] = listProperfloatingPoints
                #print(item, 'loaded')

        print("===============================")
        print('Saving the following DataFrame')
        print(df_sim)
        print("===============================")
        Bio.close_pickle(save_dict, df_rerun, df_sim, self.constant)
        return 

    def removeSimulation(self, simDict:dict):
        """
        addSimulation() removes the simulation entries from the DataFrame in the pickle file.
        
        Input:
            - simDict  : dictionary containing information about the folder names and the forcefield parameters
                         e.g. simDict = {
                                'simFolder1'  : [1.0, 1.1, 1.23],   # forcefield parameters 
                                'simFolder2' : [1.1, 1.1, 1.2]     # of the simulations
                              }
        
        Returns: 
            - None
        """
        if type(simDict) != dict:
            print("The input 'simDict' is not a dictionary. Please follow the following format - ")
            print("e.g. simDict = {")
            print("       'folderName'  : [1.0, 1.1, 1.23],   # forcefield parameters")
            print("       'folderName2' : [1.1, 1.1, 1.2]     # of the simulations")
            print("     }")
            sys.exit()

        save_dict, df_rerun, df_sim = Bio.open_pickle(self.constant)
        if len(simDict) != 0:
            for item in simDict:
                if item in df_sim.index.tolist():
                    df_sim = df_sim.drop(item)
                else: 
                    print('The entry do not exist!')
                    sys.exit()

        print("===============================")
        print('Removing the following entries')
        print(simDict)
        print("===============================")
        Bio.close_pickle(save_dict, df_rerun, df_sim, self.constant)
        return

    def addRerun(self, rerunDict:dict):
        """
        addSimulation() updates the rerun entries in the DataFrame in the pickle file 

        Input:
            - simDict  : dictionary containing information about the folder names and the forcefield parameters
                         e.g. rerunDict = {
                                'rerunfolder1'  : [1.0, 1.1, 1.23, 4.5, 3.4, 6.5],   # forcefield parameters 
                                'rerunfolder2' : [1.1, 1.1, 1.2, 6.5, 6.4, 3.3]     # of the simulations
                              }
        
        Returns: 
            - None
        """
        if type(rerunDict) != dict:
            print("The input 'rerunDict' is not a dictionary. Please follow the following format - ")
            print("e.g. rerunDict = {")
            print("       'folderName'  : [1.0, 1.1, 1.23, 4.5, 3.4, 6.5],   # forcefield parameters")
            print("       'folderName2' : [1.1, 1.1, 1.2, 6.5, 6.4, 3.3]     # of the simulations")
            print("     }")
            sys.exit()
        else: 
            size = len(rerunDict)
            if size == 0:
                print("Please provide atleast one entry!")
                sys.exit()
            else:
                listItem = [item for item in rerunDict]
                lenFF = int( int( len(rerunDict[listItem[0]]) ) /2 )
                
        save_dict, df_rerun, df_sim = Bio.open_pickle(self.constant)
        if len(rerunDict) != 0:
            for idx, item in zip(range(len(rerunDict)), rerunDict):
                ff1 = rerunDict[listItem[idx]][:lenFF] ; ff2 = rerunDict[listItem[idx]][lenFF:]
                df_rerun.loc[item] = [str(ff1), str(ff2)]
        Bio.close_pickle(save_dict, df_rerun, df_sim, self.constant)
        return 

    def removeRerun(rerunDict:dict, df_rerun):
        """
        addSimulation() removes the rerun entries from the DataFrame in the pickle file.

        Input:
            - simDict  : dictionary containing information about the folder names and the forcefield parameters
                        e.g. rerunDict = {
                                'rerunFolder1'  : [1.0, 1.1, 1.23, 4.5, 3.4, 6.5],   # forcefield parameters 
                                'rerunFolder2' : [1.1, 1.1, 1.2, 6.5, 6.4, 3.3]     # of the simulations
                            }

        Returns: 
            - None
        """
        if type(rerunDict) != dict:
            print("The input 'rerunDict' is not a dictionary. Please follow the following format - ")
            print("e.g. rerunDict = {")
            print("       'folderName'  : [1.0, 1.1, 1.23, 4.5, 3.4, 6.5],   # forcefield parameters")
            print("       'folderName2' : [1.1, 1.1, 1.2, 6.5, 6.4, 3.3]     # of the simulations")
            print("     }")
            sys.exit()
        else: 
            size = len(rerunDict)
            if size == 0:
                print("Please provide atleast one entry!")
                sys.exit()
            else:
                listItem = [item for item in rerunDict]
                lenFF = int( int( len(rerunDict[listItem[0]]) ) /2 )

        save_dict, df_rerun, df_sim = Bio.open_pickle(self.constant)
        if len(rerunDict) != 0:
            for item in rerunDict:
                if item in df_rerun.index.tolist():
                    df_rerun = df_rerun.drop(item)
                else: 
                    print('The entry do not exist!')
                    sys.exit()
        Bio.close_pickle(save_dict, df_rerun, df_sim, self.constant)
        return
        