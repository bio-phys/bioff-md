"""
Sergio Cruz, Harika Urel and Juergen Koefinger, MPI of Biophysics, Frankfurt am Main, 2022

This file contains all the function to obtained the reweighted energies for a force fiel parameter, given two know close simulations. 
It further explites the inverse tranform sampling to draw a list of observables with statistical weights 'w'.

This class requires the package pymbar from the Chodera lab:  https://pymbar.readthedocs.io/en/master/mbar.html
"""



#################################################
######### IMPORT ################################
#################################################

import numpy as np
import os
import scipy.stats
import time
import json
# mbar module from Chodera lab: 
#Python implementation of the multistate Bennett acceptance ratio (MBAR) method for estimating expectations
# and free energy differences from equilibrium samples from multiple probability densities.
import pymbar as mbar
from . import Bookeeping as bk
from . import BioFF as Bio

# gmx functions
from . import GMX_functions as gmxf

#################################################
#########Functions ##############################
#################################################

def inverse_transform_sampling(x, w, sample_size):
    # Todo: overly complicated and should be simplified
    # Code by: @Juergen Koefinger
    """
    Implements inverse transform sampling numerically. 
    See https://en.wikipedia.org/wiki/Inverse_transform_sampling
    
    Draw 'sample_size' samples from a list of observables 'x' with statistical weights 'w'. 
    
    The new sample has been drawn according to 'x' and 'w'. The returned samples have 
    weights 1/sample_size. Returned samples are not ordered. 
    
    If we want to draw another observable y, we can eigher rerun this function for y 
    or generate a sample as y' = y[idx][idx_ran]. 
    The elements of y' and x belong to the same samples.
    
    Param:
        x (array): Observable like energy.
        w (array): Weight vector for same length as 'x'. Non-negative and normalized.
        sample_size (int): Size of the sample drawn from list. Usually this number is smaller
            then the number length of 'x'. 
        
    Returns:
        x_new (array): Drawn observables.
        idx (array of int): Indices that sorts orginal samples.
        idx_ran (array of int): Indices of resampled values with respect to sorted original sample. 
    """
    print('='*24); print('inverse transform sampling x', x); print('-'*24)
    print('w ', w); print('-'*24); print('sampling size, ', sample_size); print('='*24)

    #Sort observables to calculate cumulative distributions. 
    idx=np.argsort(x)
    #Generate sorted lists. 
    x_sorted=x[idx]
    w_sorted=w[idx]
    #Calculate cumulative statistical weights. 
    w_cumu=w_sorted.cumsum()
    ran_num=np.random.uniform(size=sample_size)
    idx_ran = np.searchsorted(w_cumu, ran_num)
    print(); print('idx_ran.shape, ', idx_ran.shape); print('ran_num.shape ', ran_num.shape); print('w_cumu ', w_cumu.shape)
    #Generate samples. 
    x_new=x_sorted[idx_ran]
    return x_new, idx, idx_ran

## I have modified this code from Harika! CHECK IT!!!
def apply_mbar_get_energies(force_field_parameters, N_k, data):    
    """
    Create the correctly formated vector with all the energies for MBAR. For further information: https://pymbar.readthedocs.io/en/master/mbar.html
    
    Args:
        force_field_parameters (list): Values of the force field parameters, for which we have optimized with BioFF. It must contain at least 3 elements one below, one above and the new to test.
                                       This must be satisfied to resample with MBAR
        N_k (array, int): Number of uncorrelated snapshots sampled from state k. Some may be zero, indicating that there are no samples from that state.
    Returns:
        weights : Two-dimensional matrix of weights, one line per Hamiltonians (alpha value), length of line given by total number of simulations (N1+N2+N3). 
    """
    if N_k[-1] == 0:
        enerfile_name_dict = energy_files_dict(force_field_parameters, data)
        print(enerfile_name_dict)
        u_kn = build_MBAR_vec2(force_field_parameters, enerfile_name_dict, data)
        print('apply mbar get energies - {N_k[-1]=0} U_Kn', u_kn)
    else:
        enerfile_name_dict = build_ener_dict(force_field_parameters, data)
        print(enerfile_name_dict)
        u_kn = build_MBAR_vec(force_field_parameters, enerfile_name_dict, data)
        print('apply mbar get energies - U_kn', u_kn)

    return u_kn


#############################################################################################################################
    
def mbar_get_weights(u_kn, T, k_boltzmann, N_k):
    """
    Apply MBAR and return weights. For further information: https://pymbar.readthedocs.io/en/master/mbar.html
    
    Args:
        T (float): Single temperature for all simulations in Kelvin. 
        k_boltzmann (float): Boltzmann constant in kJ/Mol/K
        N_k (array, int): Number of uncorrelated snapshots sampled from state k. Some may be zero, indicating that there are no samples from that state.
    Returns:
        weights : Two-dimensional matrix of weights, one line per Hamiltonians (alpha value), length of line given by total number of simulations (N1+N2+N3). 
    """
    # This function pool all data for subsquent reweighting in BioFF. 
    beta = 1/(k_boltzmann*T)
    solver_options = {'maximum_iterations': 10000, 'verbose': True,}
    solver_protocol = {'options':solver_options, 'method': 'adaptive'}
    apply_mbar = mbar.MBAR(u_kn*beta, N_k, solver_protocol=(solver_protocol,))
    weights = np.transpose(apply_mbar.getWeights())
    # @Sergio:I changed this line on 15.11.2022 to use asyncmd and python3.9
    #weights = np.transpose(apply_mbar.weights())
    return weights

    
def mbar_vector(file_names_lj,number_alpha, N1=1001, N2=1001, verbose=False):  
    """
    Calculate energy matrix for MBAR.
    
    Args:
        file_names_lj (dict): Dictionary of file paths
        number_alpha: Total number of alpha values for MBAR (two simulations and one inbetween).
        N1: Number of samples from simulation at alpha1
        N2: Number of samples from simulation at alpha2
    Returns:
        u_kn: Two-dimensional matrix of energies for all alpha values (k...index of alpha value, n...index of structure).
    """
    i=0
    u_kn= np.zeros((number_alpha, N1+N2))
    #
    for idx,files in enumerate(list(file_names_lj.values())):
        if verbose:
            print("Loading the energies for MBAR")
            print(files)
        if idx!=len(list(file_names_lj.values())) :
            if idx in list(range(0,number_alpha)):
                if verbose:
                    print("Loading the energies from {} for MBAR", idx)
                # Changed this line to make the loading of files independent and handle exceptions
                energy=load_energy_from_file(files)
                # The element 1 contains the energies
                u_kn[idx,0:N1]=energy[1]
            if idx in list(range(number_alpha,number_alpha*2)):
                print("idx",idx)
                energy_diff=load_energy_from_file(files)
                u_kn[i,N1:]=energy_diff[1]
                i+=1
    return u_kn



## Rajani: This functions was modified from Harika's code, check again.
def energy_files_dict(force_field_parameters, data):   # only takes three input
    """
    Collecting file paths necessary for MBAR.
    Args:
        init_sim_path (string): Full path to initial simulations
        path_diff_base (string): Energy rerun folder for initial simulations 
        path_diff_test (string): Energy rerun folder for alpha_star with alpha1 and alpha 2
        alpha1 (string): Simulation alpha value with alpha1 the largest of the simulation alpha values smaller than new_alpha. 
        alpha2 (string): Simulation alpha value with alpha2 the smallest of the simulation alpha values larger than new_alpha. 
        alpha_star (string): Return new_alpha value rounded to two digits. 
        
    Returns:
        file_names_lj (dict): Dictionary of file paths 
    """
    enerfile_name_array = []
    for j in range(len(force_field_parameters)):
        enerfile = []
        for i in range(len(force_field_parameters[:-1])):
            if i == j:
                enerfile.append(bk.path_to_energy_file(force_field_parameters[i], data))
            else:
                path = bk.path_to_energy_file_rerun(force_field_parameters[i], force_field_parameters[j], data)
                enerfile.append(path)
        enerfile_name_array.append(enerfile)

    print(*enerfile_name_array)
    return np.array(enerfile_name_array)

def load_energy_from_file(files):
    """
    Load the energy as a function of time from the file: files
    @Sergio: In the current version, this is expected to be the output of the energy analysis of gromacs.
    Probably this should be generalized in the future
    files (str): full path to the energy file
    """
    os.chdir('/')
    energy=[]
    try:
        energy=np.transpose(np.loadtxt(files, comments=('#', '@')))
        #break
    except OSError as err:
        print("There was a problem opening the file {}!!!".format(files))
        print("Please check that this is an output .xvg file from the energy analysis of gromacs!")
        raise
        
    return energy
   
    
def rerun_energies(path, ff_param1, ff_param2, data, verbose=False, output_verbose='output.txt'):
    """
    Reruns and evaluates the energy of the trajectory with forcefield parameter 1 (ff_param1) for a new force field value (ff_param2)
    This methods creates the folder in the RERUN_FOLDER (see constants definitions).
    
    Args:
        ff_param1, (float64): Value of the forcefield parameter for which a trajectory already exists.
        ff_param2, (float64): Value of the forcefield parameter for which we want to rerun the trajectory.
    """
    # This is the path where the rerun files must go
    #path, file_name=bk.path_to_energy_folder_rerun(ff_param1, ff_param2)
    
    print('path for rerun - trial', path)
    if os.path.exists(path+'/'+data['ENERGY_FILE_NAME'])==True:
        print('since simulation exists - went inside the first part')
        os.chdir(path)
        shape = np.transpose(np.loadtxt('{}'.format(c.ENERGY_FILE_NAME), comments=['#', '@', ';'])).shape
        if shape == (2, c.NFRAMES):
            print('The {} contains the {} number of frames'.format(c.ENERGY_FILE_NAME, c.NFRAMES )) 
            return
    
    elif os.path.exists(path+'/'+data['ENERGY_FILE_NAME'])==False:
        print('Since the simulation do not exists .. went inside the second part of the code.')
        # Create folders
        
        # This create copy files rerun doesn't do anything with the force fields
        res=bk.create_copy_files_rerun(ff_param1, ff_param2, path, data)   # remove the create folder command from this defination
        print('res,', res)
        #Load gromacs and get the executable

        #gmx=gmxf.load_gmx_modules(c.GMX_MODULE)

        #Create the new itp file
        new_itp=bk.create_itp_file_new_ff_parameter(path, ff_param2, data)
        new_line='#include "'+new_itp+'"\n'

        #modify the topo.top file
        bk.replace_line_topol_file(new_line, topol_name=data['TOPOL_FILE'], line_to_replace=0)

        #Run grompp to create the new .tpr file
        gmxf.gmx_grompp(data, verbose=verbose, output_verbose=output_verbose)
        print('grompp done')
        # Copy .xtc file of the existing simulation (ff_param1), to retun the sim with the new itp file (ff_param2)
        path_sim=bk.path_to_simulation_files(ff_param1, data, initial=True)
        print(f"The path to the simulation was {path_sim+data['XTC_NAME']} and the parameter was {ff_param1}")
        bk.copyFiles(origin=path_sim+data['XTC_NAME'], destiny=path)
        xtc_force_field1=data['XTC_NAME']
        # GMX rerun 
        gmxf.gmx_mdrun_rerun(data, verbose=verbose, output_verbose=output_verbose)

        # Calculate the energy for the LJ
        gmxf.gmx_energy(data, verbose=verbose, output_verbose=output_verbose)
        
    



#############################################################################################################################
#############################################################################################################################
#############################################################################################################################

# @ Rajani: development stage. Testing needed. 
def build_MBAR_vec(force_field_parameters, enerfile_name_array, data):
    """
    Build MBAR energy vector with sampled states only
    
    Arg: 
        alpha2Bopt (list of list): values of scaling parameter alpha, for which we have optimized with BioFF.
        enerfile_name_array (array): Dictionary of paths to the energy files
        frame_number (int)       : Number of frames in the simulations
        
    Returns:
        u_kn : Two-dimensional matrix of energies for all alpha values (k ... index of alpha value, n ... index of structure).
        
    """
    frame_number = int(data['NFRAMES'])
    num_params = len(force_field_parameters)
    u_kn = np.zeros((num_params, num_params*frame_number))
    for j in range(num_params):
        for i in range(num_params):
            print(f'pair {i} & {j}')
            energy = load_energy_from_file(enerfile_name_array[j,i])[1]
            u_kn[j, i*frame_number : int(i+1)*frame_number] = energy
    #with open('U_kn.txt', 'w') as file:
    #    #file.write(f'{u_kn}')
    #    json.dump(u_kn.tolist(), file)
    return u_kn 

def build_MBAR_vec2(force_field_parameters, enerfile_name_array, data):
    """
    Build MBAR energy vector with sampled states only 

    Args; 
        - alpha2Bopts (list of list) : list of forcefield paramters discovered during the BioFF optimization.
        - enerfile_name_array (array): Array containing the paths of energy files.
        - data (dict)                : Dictionary contianing BioFF intitialization parameters.

    Returns:
        u_kn (array)                 : Two-dimensional array as matrix, for all the forcefield values (k --> index of alpha value, n --> index of structure) 
    """
    frame_number = data['NFRAMES'] ; num_params = int(len(force_field_parameters))
    u_kn = np.zeros((int(num_params), int(num_params-1)*frame_number))
    print('enerfile_name_array, ', enerfile_name_array)
    for j in range(num_params):   # one element is less from j
        for i in range(num_params-1):
            print(f'pair {i} & {j}')
            energy = load_energy_from_file(enerfile_name_array[j,i])[1]
            u_kn[j, i*frame_number : int(i+1)*frame_number] = energy
    
    print(u_kn.shape)
    return u_kn

# @Rajani: development stage. Testing needed.  
def build_ener_dict(force_field_parameters, data):
    """
    Collect energy files into dictionary for MBAR vector with only sampled states.
    
    Arg: 
        force_field_parameters (list): Value of the forcefield parameters (e.g. scaling parameter alpha), for which we have optimized the BioFF.
        
    Returns: 
        enerfile_name_array (nparray): Array of paths to energy files.
    """
    # create an empty list
    enerfile_name_array = []
    for j in range(len(force_field_parameters)):
        enerfile = []
        for i in range(len(force_field_parameters)):
            if i == j: 
                enerfile.append(bk.path_to_energy_file(force_field_parameters[i], data))
            else: 
                #path, folder_name = bk.suggest_rerun_folder()
                #rerun_energies(path, force_field_parameters[i], force_field_parameters[j], data)
                enerfile.append(bk.path_to_energy_file_rerun(force_field_parameters[i], force_field_parameters[j], data))
        enerfile_name_array.append(enerfile)

    return np.asarray(enerfile_name_array)


#############################################################################################################################
#############################################################################################################################

def exist_simulation(ff_param, simulations):
    """
    Check if a simulation with the given ff_param exist in the dataset
    """
    exist=False
    if ff_param in simulations:
        exist=True
    return exist
