# ---
# This Script is written for managing, storing, and loading the variables during the BioFF iterations using Pickle
# Author: Rajani Karmakar (Rajani.Karmakar@biophys.mpg.de or Rajani.Karmakar@ruhr-uni-bochum.de)
# ---

import numpy as np
import os
import os.path
import pickle

import shutil
import pandas as pd
import sys
#from . import * from plot_defination

#  Ask the user to choose from the following "RUN_STATE":
#  - CONTINUE or continue (use the existing pickle file and observales)
#  - RESTART or restart (start a fresh sim but save the pickle from the last run)
#  - NEW or new (start a fresh sim but reuse/rewrite the existing pickle (if any))

# Open the pickle file that uses to save the BioFF parameters. 
def open_pickle(data):
    """
    The open_pickle() func. opens the pickle file and returns the BioFF iteration data, dataFrames that contains the simulation and rerun data. 

    Inputs: 
       - path to Pickle File (optional) (str) : root + name

    Outputs: 
       - save_dict   (dict)      : Contains the interation data
       - df_rerun  (dataFrame)   : contains the rerun folder paths
       - df_sim    (dataFrame)   : contains the simulation folder paths
    """
    #print('open_pickle - data', data)
    with open('{}/pickleFiles/{}'.format(data['ROOT_DIR'], data['PICKLE_FILE']), 'rb') as f:
        save_dict = pickle.load(f)
        df_rerun  = pickle.load(f)
        df_sim    = pickle.load(f)
    return save_dict, df_rerun, df_sim

# Save the pickle file that have th BioFF parameters. 
def close_pickle(save_dict, df_rerun, df_sim, data):
    """
    The close_pickle() saves the pickle file with BioFF iteration data, dataframes of simulation and rerun information.

    Inputs: 
        - save_dict (dict) : contains the iteration data
        - df_rerun (dataframe) : contains the rerun folder paths
        - df_sim  (dataframe) : contains the simulation folder paths
    
    Returns: 
        - None
    """
    with open('{}/pickleFiles/{}'.format(data['ROOT_DIR'], data['PICKLE_FILE']), 'wb') as f:
        pickle.dump(save_dict, f)
        pickle.dump(df_rerun, f)
        pickle.dump(df_sim, f)
    return 

def read_f_func(obj):
    """
    The read_f_func() takes a pickle file which contains the threshold function and returns a python function for calculating it.

    Input: 
        - obj (PickleFile) : Pickle file  
    
    Return: 
        - pyFunction : Python function to calculate threshold function
    """
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'f_func.pickle'), 'rb') as f:
        pyFunction = pickle.load(f)
    return pyFunction

def write_f_func(func, obj):
    """
    The write_f_func() writes the pickle file and 
    """
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'f_func.pickle'), 'wb') as f:
        pickle.dump(func, f)
    return

def read_param_func(obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'param.pickle'), 'rb') as f:
        pickle_obj = pickle.load(f)
    return pickle_obj

def write_param_func(func, obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'param.pickle'), 'wb') as f:
        pickle.dump(func, f)
    return

def read_observable_func(obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'observable.pickle'), 'rb') as f:
        pickle_obj = pickle.load(f)
    return pickle_obj

def write_observable_func(func, obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'observable.pickle'), 'wb') as f:
        pickle.dump(func, f)
    return

def read_expObservable(obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'expObservable.pickle'), 'rb') as f:
        pickle_obj = pickle.load(f)
    return pickle_obj

def write_expObservable(data, obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'expObservable.pickle'), 'wb') as f:
        pickle.dump(data, f)
    return

def read_function_for_scaling_itp(obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'scale_itp.pickle'), 'rb') as f:
        pickle_obj = pickle.load(f)
    return pickle_obj

def write_function_for_scaling_itp(data, obj):
    with open('{}/pickleFiles/{}'.format(obj['ROOT_DIR'], 'scale_itp.pickle'), 'wb') as f:
        pickle.dump(data, f)
    return

def param_prior(ff_param, ref_param):
    """ No Param. x is list."""
    return 0

def initialize_dict():
    save_dict = {
        'time_optimization':[], 'time_recal_energy':[], 'param_prior': [],
        'time_reweighting':[], 'time_iteration':[],'ff_params':[],
        'w0':[], 'w':[], 'yTilde':[], 'YTilde': [], 'all_u':[],
        'all_u_refs':[], 'theta':[], 'kish_tot':[], 'kish_num_update':[],
        'SKL_val': [], 'L_val': [], 'chi2_val':[], 'OBJ_val':[], 'f_func':[]
    }
    return save_dict

def open_synthetic_df(data):
    path = data['CHECKPOINT']
    
    if data['RUN_STATE'] == 'RESTART':
        csvFile = pd.read_csv(path+'/synthetic_simulation.csv', index_col=False)
        return csvFile
    
    else:
        # Initialize the rerun dataframe
        df = {}
        nff = data['NO_OF_FF_PARAMETER']        # find best way to take this data from user?? How user will decide which FF_parameter to use. 
        for i in range(int(nff)):
            df['ff{}'.format(int(i+1))] = []
        df = pd.DataFrame(df)
        df.to_csv(path+'/synthetic_simulation.csv', index=False)
        return df
        

def close_synthetic_df(df, data):
    path = data['CHECKPOINT']
    df.to_csv(path+'/synthetic_simulation.csv', index=False)
    return 

def Initialize(data):
    # initialize the dictionary with quantities that are being saved.
    save_dict = initialize_dict()
    
    # Initialize the rerun dataframe
    data_rerun = {}
    nff = data['NO_OF_FF_PARAMETER']        # find best way to take this data from user?? How user will decide which FF_parameter to use. 
    for i in range(int(nff)):
        data_rerun['ff{}'.format(int(i+1))] = []
    for i in range(int(nff)):
        data_rerun['ff_rerun{}'.format(int(i+1))] = []
    #data_rerun['path'] = []
    df_rerun=pd.DataFrame(data_rerun)
    
    # Initialize the simulation dataframe
    data_sim = {}
    nff = data['NO_OF_FF_PARAMETER']
    for i in range(int(nff)):
        data_sim['ff{}'.format(int(i+1))] = []
    #data_sim['path']=[]
    df_sim=pd.DataFrame(data_sim)

    # save it in pickle
    close_pickle(save_dict, df_rerun, df_sim, data) 
    
def restart_state(data):
    save_dict, df_rerun, df_sim = open_pickle(data)
    save_dict = initialize_dict() 
    close_pickle(save_dict, df_rerun, df_sim, data)
    
def Constructor(data):
    """
    Constructor() defines/construct the state of the run based on input from the user
    """
    if 'NEW' in data['RUN_STATE'] or 'new' in data['RUN_STATE']:                       # If New
        Initialize(data)
        #fill_sim_df2()

    #elif 'CONTINUE' in C.RUN_STATE or 'continue' in C.RUN_STATE:           # If continue 
    #    save_dict, df_rerun, df_sim = open_pickle()
    #    close_pickle(save_dict, df_rerun, df_sim)

    elif 'RESTART' in data['RUN_STATE'] or 'restart' in data['RUN_STATE']:    # If Restart
        restart_state(data)

    else:
        print("Please specify a valid option for 'RUN_STATE' - restart or new ")
        sys.exit()

    #else:                                                   # Scream to provide a name
    #    raise 'Please check if you have defined the state correctly'
        
        
## -------------------------------------
#  Saving parameters in the pickle file
def savePickleObjects(objects, nameObj, data):
    """
    savePickleObjects() saves the parameters in the outer iterations. The following iterations are saved - 
        - ff_params          : force field parameters that are optimized in BioFF
        - time_optimization  : Optimization time for every outer iteration
        - time_reweighting   : Reweighting time in every outer iteration
        - time_recal_energy  : Time for computing energies from trajectories
        - w0                 : reference weights
        - yTilde             : computed yTilde
        - YTilde             : YTilde
        - kish_tot           : kish sampling size
        - all_u_refs         : reference energy 
        - theta              : confidence parameter

    Input:
        - objects: contains the parameter values to be saved in the pickle file.
        - nameObj: contains the name of parameter in the file. 

    Returns:
        - None
    """
    save_dict, df_rerun, df_sim = open_pickle(data)
    save_dict[nameObj].append(objects)
    close_pickle(save_dict, df_rerun, df_sim, data)


def savePickleObjectsInner(objects, nameObj, data):
    """
    savePickleObjectsInner() saves the parameters in the Inner iterations. The following iterations are saved - 
        - w            : weights of structures from the pool of simulations
        - SKL_val      : Kullback-Liebler Divergence 
        - L_val        : Likelihood value
        - param_prior  : Parameter prior used
        - chi2_val     : chi-square value 
        - kish_tot     : kish sampling size 
        - OBJ_val      : Objective function value
        - all_u        : energy of all the trajectory combined
        - f_func       : threshold function value

    Input:
        - objects: contains the parameter values to be saved in the pickle file.
        - nameObj: contains the name of parameter in the file. 

    Returns:
        - None
    """
    save_dict, df_rerun, df_sim = open_pickle(data)
    save_dict[nameObj][-1].append(objects)
    close_pickle(save_dict, df_rerun, df_sim, data)

def save_rerun_df(objects, param1, param2, data):
    """
    save_rerun_df() saves the forcefield parameters used for the rerun in a DataFrame -
    
    Input: 
        - objects : contains the name of the folder where the rerun will be computed
        - param1  : forcefield parameter for which the simulation files are used in the rerun  
        - param2  : forcefield parameter for rerun

    Returns: 
        - None
    """
    save_dict, df_rerun, df_sim = open_pickle(data)
    ff = param1 + param2
    df_rerun.loc[int(objects)] = ff # add a command to check if the len of ff = DF
    close_pickle(save_dict, df_rerun, df_sim, data)

def save_sim_df(objects, param_list, data):
    """
    save_sim_df() saves the forcefield parameters at which new simulations are run - 

    Input: 
        - objects    : Name of the folder where the simulations will be stored
        - param_list : forcefield parameter at which the new simulation is started

    Returns: 
        - None  
    """
    save_dict, df_rerun, df_sim = open_pickle(data)
    df_sim.loc[int(objects)] = param_list
    close_pickle(save_dict, df_rerun, df_sim, data)

