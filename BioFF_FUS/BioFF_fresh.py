#!/usr/bin/env python
# coding=utf-8
###################################################################################
import numpy as np
import os
import os.path
import matplotlib.pyplot as plt
import matplotlib
import time
import pymbar as mbar

## Pieces of code
# Import the constants
from . import Bookeeping as bk
from . import MBAR_and_inverseSampling as mbis

from . import GMX_functions as gmxf
from . import Synthetic_sim as ssim
from . import BioFF_von_Mises as bioff
from . import BioFF as Bio

#pandas
import pandas as pd

# For running BioFF
import scipy.stats

import time
import itertools

import asyncio
import MDAnalysis as mda

import asyncmd
from asyncmd import gromacs as asyncgmx
from asyncmd import trajectory as asynctraj

# ------------
## Define the Likelihood or negative log-posterior
def neg_log_posterior(w, w0, yTilde, YTilde, theta, param_prior, stderr):
    """
    The BioEn negative log-posterior. 
    """
    SKL_val = bioff.SKL(w, w0)
    chi2_val = bioff.chi2(w, yTilde, YTilde, stderr)
    return -np.log(param_prior) + theta*SKL_val + chi2_val/2., SKL_val, chi2_val

##Define a function to prepare the rerun of energies and MBAR
def chNamesOutputFiles(data, wdir, name_job):
    os.chdir(wdir)
    XTC = data['XTC_NAME']
    TPR = data['TPR_NAME']
    ENER = data['ENER_EDR']

    os.system(" mv {}.part0001.xtc {}".format(name_job, XTC))
    os.system(" mv {}.tpr {}".format(name_job, TPR))
    os.system(" mv {}.part0001.edr {}".format(name_job, ENER))


async def new_s_asyncmd(MDP_file, GRO_file, TOP_file, SBATCH_script, wdirs, deffnm, force_field_new, n_steps=50000000,n_simulations=1):
    """
    Inspired in examples from https://gitea.kotspeicher.de/AIMMD/asyncmd
    This prepares and submit a new simulation to the cluster with the specified parameters
    This function builds on the capabilities of asyncmd (https://gitea.kotspeicher.de/AIMMD/asyncmd) , and it is designed to use Gromacs. 
    --------------
    Parameters:
    MDP_file= (str) Path to molecular dynamics parameter file (.mpd), 
    GRO_file= (str) Initial structure of the simulation file (.gro), 
    TOP_file=  (str)Topology file (.top), 
    SBATCH_script = (str) SBATCH script - see an example in /scripts/mdrun_MASTER_bio.slurm, 
    wdirs =  (str) working directory (full path), 
    deffnm =  (str)name for the output files of the simulation (see also Gromacs manual),.
    force_field_new = (double) value of the new force-field,
    n_steps=(int) default: 50000000. Number of simulation steps
    n_simulations=(int) default:1
    """
     
    start0 = time.time()

    print(f'----------Starting simulation for alpha={force_field_new}')
    
    n_engines=1
    # one MDP object per engine, in principal we could use the same object but this way is more customizable,
    # e.g. we could want to modify our setup have the engines run at a different temperatures
    mdps = [asyncgmx.MDP(MDP_file) for _ in range(n_engines)]

    # @Sergio_ I include this line to create any .trr file and avoid the error, ask Hendrick
    mdps[0]['nsteps']=n_steps #20000000 #50000
    mdps[0]['nstvout']=int(mdps[0]['nsteps']/4)
    # Let us create a list of identical engines to showcase the power of concurrent execution :)
    engines = [asyncgmx.SlurmGmxEngine(mdconfig=mdps[0],
                                       gro_file=GRO_file, #"../test1/alpha_1.0000/npt_12ns.gro",  # required
                                       top_file=TOP_file, #"../test1/alpha_1.0000/topol.top",  # required
                                       # NOTE this is the only additional thing needed for using the SlurmGmxEnigne w.r.t. GmxEngine 
                                       sbatch_script=SBATCH_script, #"mdrun_MASTER_bio.slurm", # required!
                                       output_traj_type="xtc",
                                       # optional (can be omited or None), however naturally without an index file
                                       # you can not reference custom groups in the .mdp-file or MDP object 
                                       #ndx_file="gmx_infiles/capped_alanine_dipeptide/index.ndx",
                                       # limit each engine to 2 threads (the box is so small that otherwise 
                                       # the domain decomposition fails)
                                       #mdrun_extra_args="-nt 2",  # use this if your version of GMX is compiled with thread-MPI support
                                       #mdrun_extra_args="-ntomp 2",  # use this for GMX without thread-MPI support
                                      )
               for mdp in mdps]

    e0 = engines[0]  # get it out of the list so tab-help/completion works

    # the prepare method is an async def function (a coroutine) and must be awaited
    await e0.prepare(starting_configuration=None, workdir=wdirs, deffnm=deffnm)

    # run all engines at once and time it
    start = time.time()
    nsteps=mdps[0]['nsteps']
    print("steps:", nsteps )
    trajs = await asyncio.gather(*(e.run_steps(nsteps=n_steps, ) for e in engines))
    #trajs = await asyncio.gather(*(e.run(walltime=1.5, nsteps=n_steps,) for e in engines))
    
    end = time.time()

    print(f"Running simulation took {round(end - start, 4)} seconds.")

    end0 = time.time()

    print(f"Total time for {n_simulations} simulation(s) was:  {round(end0 - start0, 4)} seconds.")

## FUNCTIONS
max_iterations = 50
kish_tot = np.ones((max_iterations,50))*-1
def Num(data):
    save_dict, df_rerun, df_sim = Bio.open_pickle(data)
    save_dict['SKL_val'].append([])
    save_dict['chi2_val'].append([])
    save_dict['L_val'].append([])
    save_dict['OBJ_val'].append([])
    save_dict['kish_tot'].append([])
    save_dict['w'].append([])
    save_dict['all_u'].append([])
    save_dict['f_func'].append([])
    save_dict['param_prior'].append([])
    save_dict['time_recal_energy'].append([])
    save_dict['time_reweighting'].append([])
    Bio.close_pickle(save_dict, df_rerun, df_sim, data)


## Objective Function

#
def objective_function(ff_param, ref_param, ff_param_old, w0, yTilde, YTilde,  all_u_refs, theta, data, stderr, n_threshold = 300, k_threshold=0.4):
    """
    Objective function for BioFF optimization.
    
    Args: 
        alpha (float): The current scaling parameter of the LJ pair-pair interaction. It should be replaced by dictionary for multiple parameters. 
        w0: (array): The reference weights and the reference alpha-value
        all_u (array): All energies for the current value of alpha
        yTilde (array): The calculated observables for all members in the ensemble divided by the total error
        YTilde (float): Experimental average value of the observable 
        theta (float): Trading off experimental and theoretical information (BioEn)
        n_threshold (int): Kish sample size parameter threshold 
        k_threshold (float): Parameter set to avoid leaving sample space by setting threshold on Kish' sample size (BioFF paper)
        
    Return:
        
    """
    # add calculation of all_u for current alpha value 
    start_time2 = time.time()
    print("Let's try the value", ff_param)
    all_u = recalculate_energy(ff_param, ff_param_old, data)                            ##### /10
    print('all_u', all_u)
    print("length_all_u",len(all_u))
    print("mean_all_u", np.mean(all_u))
    
    Bio.savePickleObjectsInner(float(time.time()- start_time2), 'time_recal_energy', data)
    Bio.savePickleObjectsInner(all_u, 'all_u', data)
    
    start_time3 = time.time()
    w = bioff.reweighting(data, all_u, all_u_refs, w0)
    Bio.savePickleObjectsInner(float(time.time()-start_time3), 'time_reweighting', data)
    Bio.savePickleObjectsInner(w, 'w', data)

    # call the threshold function and parameter prior
    f_threshold = Bio.read_f_func(data)
    param_prior_func = Bio.read_param_func(data)
    # calculate the parameter_prior
    param_prior = param_prior_func(ff_param, ref_param)
    print('Parameter Prior value, ', param_prior )

    print("w", w)
    print("w-shape", w.shape)
    print('stderr', stderr)
    L_val, SKL_val, chi2_val = neg_log_posterior(w, w0, yTilde, YTilde, theta, param_prior, stderr)
    print("Lval", L_val)
    print('SKL_val', SKL_val)
    print('chi2_val', chi2_val)
    n = bioff.effective_sample_size(w)
    
    #Include to keep track of the kish sampling size
    Bio.savePickleObjectsInner(param_prior, 'param_prior', data)
    Bio.savePickleObjectsInner(SKL_val, 'SKL_val', data)
    Bio.savePickleObjectsInner(L_val, 'L_val', data)
    Bio.savePickleObjectsInner(chi2_val, 'chi2_val', data)
    Bio.savePickleObjectsInner(n, 'kish_tot', data)
    Bio.savePickleObjectsInner(L_val*f_threshold(n, n_threshold, k_threshold), 'OBJ_val', data)
    Bio.savePickleObjectsInner(f_threshold(n, n_threshold, k_threshold), 'f_func', data)
    
    print("n", n)
    print( L_val*f_threshold(n, n_threshold, k_threshold))
    return L_val*f_threshold(n, n_threshold, k_threshold)


def recalculate_energy(ff_parameter_value_new, ff_param_old, data):
    """
    Recalculates the energy with the new ff_parameter for the configurations obtained with the ff_param_old
    -----------------------------------------
    Parameters
    ff_parameter_value_new (float)
    ff_param_old (float)
    -----------------------------------------
    Return
    new energies
    """
    # create an empty numpy array to store the differences in energy
    x_new_ener=[]
    
    # Iterates over the old parameters to rerun the energies
    for ff_parameter in ff_param_old:
        ff_parameter_list = [ff_parameter, ff_parameter_value_new]
        print ('The ff values are', ff_parameter_list)
        path=bk.path_to_energy_file_rerun(ff_parameter_list[0], ff_parameter_list[1], data)
        print('Path recalculate Energy, ', path)
        #mbis.rerun_energies(path, ff_parameter_list[0], ff_parameter_list[1], data, verbose=True )
        #file=bk.path_to_energy_file_rerun(ff_parameter_list[0], ff_parameter_list[1])
        #file=path+'/'+c.ENERGY_FILE_NAME
        x_ener = mbis.load_energy_from_file(path)
        x_new_ener = np.append(x_new_ener, np.asarray(x_ener[1]))

    return x_new_ener

def mean_Rg(Rgs):
    """
    Calculates the mean_Rg
    """
    return np.sqrt((Rgs**2).mean())

def initialize_params(data):
    #sbatch master
    # @Sergio: This could be send to the constants file

    sbatch=data['SBATCH_MASTER']
    
    ## Define alpha
    #alpha value for initial (or reference) simulation from the user data
    alpha_0 = data['FF_PARAM']    #  [1.0]#[0.8999999999999999, 1.0093750000000004] #args.alpha_0
    #alpha value for synthetic experimental data
    #alpha_exp = data['FF_EXP']    # [1.6] #args.alpha_exp #0.7 
    
    ##Define parameters for BioFF - run
    theta = data['THETA'] #args.theta
    
    # run constructor that build a new pickle file with a proper structure inside the pickle file
    #Bio.Constructor(data)
    
    print('----------------------------------------------------------------------------')
    print('--------------------- Read synthetic data for experiment   -----------------')
    print('----------------------------------------------------------------------------')
    
    # Load the obsevable (radius of gyration) for the initial value of the force-field
    path=bk.path_to_simulation_files(alpha_0, data)
    print(f'path to simulation files {path}')

    # Load from pickle
    observable_func = Bio.read_observable_func(data)
    # calculate the Observable of ensemble in np.array at the given path 
    observable = observable_func(path)
    
    #print("The radius of gyration predicted by current ff is {}".format(observable))
    # yTlide is an input for BioFF - see Eq. (10)
    y = np.asarray(observable)
    stderr=1
    yTilde = y/stderr
    print(f"yTilde = {yTilde}")
    print(f"yTilde shape = {yTilde.shape}")
    
    # Load the energy (Here only LJ)
    path=bk.path_to_energy_file(alpha_0, data)
    print('path for alpha_0, ', path)
    energy_alpha_0_ts=mbis.load_energy_from_file(path) 
    energy_alpha_0=energy_alpha_0_ts[1]                                    ####### /10
    #
    ## all_u_refs is the input for BioFF
    all_u_refs = np.asarray(energy_alpha_0)
    print('all_u_refs,', all_u_refs)
    print('all_u_refs.shape, ', all_u_refs.shape)
    
    # Load the obsevables (radius of gyration) for the experiment.
    #path=bk.path_to_simulation_files(alpha_exp)
    
    # load from pickle 
    expObservable, stderr = Bio.read_expObservable(data)
    print("The 'experimental' Observable is {}".format(expObservable))
    print(f"The experimental Standard Deviation is {stderr}")
    ## We need the experimental average value YTilde - input for BioFF - see Eq. (10) - paper. 
    YTilde = np.asarray(expObservable)
    stderr = np.asarray(stderr)
    print(f"YTilde = {YTilde}")
    print(f"YTilde.shape {YTilde.shape}")
    
    print('----------------------------------------------------------------------------')
    print('--------------------- Initial weights (w0)  --------------------------------')
    print('----------------------------------------------------------------------------')
    #
    
    N_w = int(data['NFRAMES'])
    w0 = np.ones(N_w)/N_w
    print(N_w)
    print(w0)
    
    alpha_new = alpha_0 # Starts with the current value of the ff-
    #list for all optimized alpha valuesa
    alphas2Bopt = [alpha_new] 
    print('='*24)
    print('The Initialization part is finished')
    print('='*24)
    return alpha_new, alphas2Bopt, w0, yTilde, YTilde, all_u_refs, theta, y, stderr

# First Run()
def firstRun(alpha_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, y, data, stderr):
    #
    print('----------------------------------------------------------------------------')
    print('--------------------- First run  -------------------------------------------')
    print('----------------------------------------------------------------------------')
    #
    print(f'stderr {stderr}')
    Bio.savePickleObjects(alpha_new, 'ff_params', data)
    Bio.savePickleObjects(w0, 'w0', data)
    Bio.savePickleObjects(YTilde, 'YTilde', data)
    Bio.savePickleObjects(all_u_refs, 'all_u_refs', data)
    Bio.savePickleObjects(theta, 'theta', data)
    Bio.savePickleObjects(yTilde, 'yTilde', data)

    Num(data)
    start_time = time.time()
    res = scipy.optimize.minimize(objective_function, alpha_new, args=(alpha_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, data, stderr,), method="Nelder-Mead", tol=.01, options={'maxiter':20})
    print('------', res, '------')
    print("optimization time in seconds",time.time()-start_time)
    Bio.savePickleObjects(float(time.time()-start_time), 'time_optimization', data)
    
    ### Append it to the list of observable
    alpha_new = res['x'].tolist()
    print(alpha_new)

    alphas2Bopt.append(alpha_new)
    Bio.savePickleObjects(alpha_new, 'ff_params', data)
    return alpha_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, y, stderr

# main function Iteration until convergence of BioFF

async def main(force_field_new, alphas2Bopt, w0, yTilde, YTilde,  all_u_refs, theta, y, data, stderr):
    print('----------------------------------------------------------------------------')
    print('--------------------- Starting main iteration--------------------------------')
    print('--------------------- Starting main iteration--------------------------------')
    print('----------------------------------------------------------------------------')

    sbatch=data['SBATCH_MASTER']
    times=[]
    n_iterations = 0
    max_iterations = 50 #maximum number of BioFF iterations; at most to the length of alpha grid use for iteration
    is_converged = False

    start_time0 = time.time()

    #while is_converged == False or n_iterations <= max_iterations:
    for i in range(0, max_iterations):
        print('----------------------------------------------------------------------------')
        print('--------------------- We will create a new simulation -----------------------')
        print('----------------------alpha={}, iteration {}'.format(force_field_new, n_iterations))
        print('----------------------------------------------------------------------------')
        #Recalculate energy
        #idx_ran_energ, ff_parameter_list=recalculate_energy_for_synthetic_sim(force_field_new)

        # Create a synthetic simulations
        #ssim.create_synthetic_simulation(ff_parameter_list, idx_ran_energ)

        #force_field_new=alpha_new
        wdirs=bk.path_to_simulation_files(force_field_new, data)   

        # Prepare names for asyncmd
        ffnew_formatted = []
        for item in force_field_new:
            ffnew_formatted.append(bk.to_str(item))
        name_job="bioff"

        if os.path.exists(wdirs)==False:
            # Prepare the files for the new simulation with the new force field parameter
            force_field_new, mdp_file, gro_file, topol_file=ssim.prepare_new_simulation(force_field_new, data)

            #Submit the simulation to the cluster using asyncmd
            print('----------------------------------------------------------------------------')
            print('--------------------- We are waiting for the simulation to finish --------')
            print('----------------------Take a look at your cluster queue --------')
            print('----------------------------------------------------------------------------')

            await new_s_asyncmd(MDP_file=mdp_file, GRO_file=gro_file, TOP_file=topol_file, SBATCH_script=sbatch, 
                          wdirs=wdirs, deffnm=name_job, force_field_new=force_field_new,
                          n_steps=50000000,n_simulations=1)
            
        #Change the names of the files in the simulation folder for further processing
        chNamesOutputFiles(data, wdir=wdirs, name_job=name_job)
        
        #Calculate the energy of the 
        gmxf.gmx_mdrun_rerun(data)
        gmxf.gmx_energy(data)

        xtc_file=data['XTC_NAME']
        tpr_file=data['TPR_NAME']
        
        output_xtc='md_pbcmol_centered.xtc'   # This value should come from the constant File

        os.chdir(wdirs)

        gmxf.gmx_unwrap_xtc(data)

        # Copy this file to calculate the Rg - This is of course only true for the current observable.
        os.system("cp {} {}".format(data['INIAL_GRO_FILE_NOWATER'], wdirs))

        # Load from pickle
        observable_func = Bio.read_observable_func(data)
        # calculate the Observable of ensemble in np.array at the given path 
        #path = bk.path_to_simulation_files(force_field_new)
        observable = np.asarray(observable_func(wdirs))
        
        print (f'---------------{np.mean(observable, axis=0)}, it contains {observable.shape} values-------------')
        

        yTilde = np.append(yTilde, np.asarray(observable), axis=1)
        print("yTilde.shape",yTilde.shape)
        print(f"yTilde = {yTilde}")



        print(alphas2Bopt)

        #print('--------------------- Running LJ-Energy fot the new simulation -------------')
        #print('----------------------------------------------------------------------------')

        #gmxf.gmx_energy(group=str(40), gmx=c.GMX, ener_file='ener.edr', output=c.ENERGY_FILE_NAME, verbose=False, output_verbose='output.txt') 

        print('--------------------- Append the observables from the new simulation-----------')
        print('----------------------------------------------------------------------------')


        ###Calculate the new weigths
        N_w = [int(data['NFRAMES'])] * len(alphas2Bopt)
        u_kn_syn = mbis.apply_mbar_get_energies(alphas2Bopt, N_w, data)
        all_u_refs = u_kn_syn[0]                                                   ### /10
        weights_syn=mbis.mbar_get_weights(u_kn_syn, c.T, c.K_BOLTZMANN, N_w)
        w0=weights_syn[0]

       # Bio.save_ff_params(force_field_new)
        Bio.savePickleObjects(w0, 'w0', data)
        Bio.savePickleObjects(YTilde, 'YTilde', data)
        Bio.savePickleObjects(all_u_refs, 'all_u_refs', data)
        Bio.savePickleObjects(yTilde, 'yTilde', data)
        Num()
        
        start_time = time.time()
        res = scipy.optimize.minimize(
            objective_function, 
            force_field_new, 
            args= (
                force_field_new, 
                alphas2Bopt, 
                w0, 
                yTilde, 
                YTilde, 
                all_u_refs, 
                theta, 
                data, 
                stderr
            ), 
            method="Nelder-Mead", 
            tol=.01, 
            options={'maxiter':20}
        )
        print('------', res, '------')
        print("optimization time in seconds",time.time()-start_time)                     ### HERE Serious modification needed
        Bio.savePickleObjects(float(time.time()-start_time), 'time_optimization', data)

        #Append time of this iteration
        times.append(time.time()-start_time)

        alpha_old = force_field_new
        force_field_new = res['x'].tolist()
        
        alphas2Bopt.append(force_field_new)
        Bio.savePickleObjects(force_field_new, 'ff_params', data)
        n_iterations+=1

        #Convergence condition
        if bioff.convergence(alpha_old, force_field_new, threshold=0.005):
            is_converged = True
            break

    print("The TOTAL optimization time in seconds:",time.time()-start_time0)




