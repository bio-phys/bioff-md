"""
Sergio Cruz-Leon, Harika Urel and Juergen Koefinger, MPI of Biophysics, Frankfurt am Main, 2022

Observables to calculate for the simualtions. This class was created as a general growing pool of functions to analyze the simulations.

"""
#################################################
######### IMPORT #############################
#################################################
import numpy as np

import asyncio
import MDAnalysis as mda
import asyncmd
from asyncmd import gromacs as asyncgmx
from asyncmd import trajectory as asynctraj

import time
#################################################
#########Functions ##############################
#################################################

async def new_s_asyncmd(MDP_file, GRO_file, TOP_file, SBATCH_script, wdirs, deffnm, force_field_new, n_steps=50000000,n_simulations=1):
    """
    Inspired in examples from https://gitea.kotspeicher.de/AIMMD/asyncmd
    This prepares and submit a new simulation to the cluster with the specified parameters
    This function builds on the capabilities of asyncmd (https://gitea.kotspeicher.de/AIMMD/asyncmd) , and it is designed to use Gromacs. 
    --------------
    Parameters:
    MDP_file= (str) Path to molecular dynamics parameter file (.mpd), 
    GRO_file= (str) Initial structure of the simulation file (.gro), 
    TOP_file=  (str)Topology file (.top), 
    SBATCH_script = (str) SBATCH script - see an example in /scripts/mdrun_MASTER_bio.slurm, 
    wdirs =  (str) working directory (full path), 
    deffnm =  (str)name for the output files of the simulation (see also Gromacs manual),.
    force_field_new = (double) value of the new force-field,
    n_steps=(int) default: 50000000. Number of simulation steps
    n_simulations=(int) default:1
    """
    start0 = time.time()

    print(f'----------Starting simulation for alpha={force_field_new}')
    
    n_engines=1
    # one MDP object per engine, in principal we could use the same object but this way is more customizable,
    # e.g. we could want to modify our setup have the engines run at a different temperatures
    mdps = [asyncgmx.MDP(MDP_file) for _ in range(n_engines)]

    # @Sergio_ I include this line to create any .trr file and avoid the error, ask Hendrick
    mdps[0]['nsteps']=n_steps #20000000 #50000
    mdps[0]['nstvout']=int(mdps[0]['nsteps']/4)
    # Let us create a list of identical engines to showcase the power of concurrent execution :)
    engines = [asyncgmx.SlurmGmxEngine(mdconfig=mdps[0],
                                       gro_file=GRO_file, #"../test1/alpha_1.0000/npt_12ns.gro",  # required
                                       top_file=TOP_file, #"../test1/alpha_1.0000/topol.top",  # required
                                       # NOTE this is the only additional thing needed for using the SlurmGmxEnigne w.r.t. GmxEngine 
                                       sbatch_script=SBATCH_script, #"mdrun_MASTER_bio.slurm", # required!
                                       output_traj_type="xtc",
                                       # optional (can be omited or None), however naturally without an index file
                                       # you can not reference custom groups in the .mdp-file or MDP object 
                                       #ndx_file="gmx_infiles/capped_alanine_dipeptide/index.ndx",
                                       # limit each engine to 2 threads (the box is so small that otherwise 
                                       # the domain decomposition fails)
                                       #mdrun_extra_args="-nt 2",  # use this if your version of GMX is compiled with thread-MPI support
                                       #mdrun_extra_args="-ntomp 2",  # use this for GMX without thread-MPI support
                                      )
               for mdp in mdps]

    e0 = engines[0]  # get it out of the list so tab-help/completion works

    # the prepare method is an async def function (a coroutine) and must be awaited
    await e0.prepare(starting_configuration=None, workdir=wdirs, deffnm=deffnm)

    # run all engines at once and time it
    start = time.time()
    
    print(f"The simulation will run for {n_steps} steps")
    trajs = await asyncio.gather(*(e.run_steps(nsteps=n_steps, ) for e in engines))
    #trajs = await asyncio.gather(*(e.run(walltime=1.5, nsteps=n_steps,) for e in engines))
    
    end = time.time()

    print(f"Running simulation took {round(end - start, 4)} seconds.")

    end0 = time.time()

    print(f"Total time for {n_simulations} simulation(s) was:  {round(end0 - start0, 4)} seconds.")


