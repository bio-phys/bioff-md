"""
Sergio Cruz, Harika Urel and Juergen Koefinger, MPI of Biophysics, Frankfurt am Main, 2022

Bookkeeping functions. To deal with paths, folders, etc

"""
#################################################
######### IMPORT #############################
#################################################
import numpy as np
import os
import os.path
import pandas as pd
import scipy.stats
import time
from . import BioFF as Bio
from . import MBAR_and_inverseSampling as mbis


#################################################
#########Functions ##############################
#################################################
#---------------------------------------------------
#---------------------------------------------------
def path_to_energy_file(ff_list_value, data, initial=True):
    """
    Returns the full path to the energy file of the ff parameter: ff_list_value
    Initially implemented for the initial simulations.
    
    @Sergio: Need to be extended for the case where initial=False/ Need other functions to handle the creation of new folders
    @Rajani: can be used to 
    """
    path=""
    param_list=[]
    for i in range(len(ff_list_value)):
        val1=to_str(ff_list_value[i], data['DECIMAL_PLACES'])
        param_list.append('{}'.format(val1))
        
    save_dict, df_rerun, df_sim = Bio.open_pickle(data)
    index_sim = check_sim(param_list, df_sim, data)      # param is list, checks that the
    Bio.close_pickle(save_dict, df_rerun, df_sim, data)
    
    if len(index_sim)==0:
        print('Simulation does not exists !')
        # Suggest new folder name
        path, folder_name = suggest_sim_folder(data)
        save_dict, df_rerun, df_sim = Bio.open_pickle(data)
        #Bio.save_sim_df(int(folder_name), param_list)        #ADDDDDDD THE proper import for calling BioFF_save.py
        Bio.close_pickle(save_dict, df_rerun, df_sim, data)
        return data['ROOT_DIR']+data['PROJECT']+data['INITIAL_SIMULATIONS']+'/'+str(folder_name)+'/'+data['ENERGY_FILE_NAME']

    elif len(index_sim)==1:
        print('Simulation exists!')
        [folder_name] = index_sim         # Here 'path' is name of the folder.
        return data['ROOT_DIR']+data['PROJECT']+data['INITIAL_SIMULATIONS']+'/'+str(folder_name)+'/'+data['ENERGY_FILE_NAME']

    else:
        print('----')
        print('There are possibly multiple copies of entries for same simulation in the DataFrame: df_sim inside the pickle file.') 
        print('We are taking the first entry found with the particular set of ff_paramters')
        print('----')
        folder_name = index_sim[0]
        return data['ROOT_DIR']+data['PROJECT']+data['INITIAL_SIMULATIONS']+'/'+str(folder_name)+'/'+data['ENERGY_FILE_NAME']

def list2list(ff_list_value, data):
    """ """
    param_list = []
    for i in range(len(ff_list_value)):
        val1=to_str(ff_list_value[i], data['DECIMAL_PLACES'])
        param_list.append('{}'.format(val1))
    return param_list

# Checkpoint entries in dataframes are removed

#---------------------------------------------------
#---------------------------------------------------
def path_to_simulation_files(ff_list_value, data, initial=True, byPassCheck=False):
    """
    Returns the full path to the energy file of the ff parameter: ff_parameter_value
    Initially implemented for the initial simulations
    @Sergio: Need to be extended for the case where initial=False/ Need other functions to handle the creation of new folders
    """
    param_list=list2list(ff_list_value, data)
    
    path=""
    # If the folders where created with this code, the energy file must be here:
    root=data['ROOT_DIR']+data['PROJECT']+data['INITIAL_SIMULATIONS']

    if not byPassCheck:
        save_dict, df_rerun, df_sim = Bio.open_pickle(data)
        index_sim = check_sim(param_list, df_sim, data)      # param is list, checks that the
        Bio.close_pickle(save_dict, df_rerun, df_sim, data)
        #[folder_name]= index_sim
    if byPassCheck:
        index_sim=[]
    
    print('param',  param_list)
    if len(index_sim)==0:
        print('Simulation does not exists !')
        # Suggest new folder name
        path, folder_name = suggest_sim_folder(data)
        save_dict, df_rerun, df_sim = Bio.open_pickle(data)
        df_sim.loc[folder_name] = param_list
        Bio.close_pickle(save_dict, df_rerun, df_sim, data)
        #Bio.save_rerun_df(int(folder_name), param1, param2)                # should we save to the data frame now or later??
        return path                 # Here, path is the path of the folder itself

    elif len(index_sim)==1:
        print('Simulation exists!')
        [path] = index_sim         # Here 'path' is name of the folder.
        return root+'/'+str(path)+'/'

    else:
        print('----')
        print('There is a possibility of multiple copies of simulations is available in the dataFrame.')
        print('We are taking the first entry found with the particular set of ff_parameters')
        print('----')
        path = index_sim[0]
        return root+'/'+str(path)+'/'   # Here, path is name of the folder


#---------------------------------------------------
#---------------------------------------------------
def path_to_energy_file_rerun(ff_parameter_value, ff_parameter_value2, data, initial=True):
    """
    Returns the full path to the energy file of the ff parameter: ff_parameter_value
    Initially implemented for the initial simulations
    Need to be extended for the case where initial=False/ Need other functions to handle the creation of new folders
    """
    
    path=""
    # If the folders where created with this code, the energy file must be here:
    
    if initial:
        path= path_to_energy_folder_rerun(ff_parameter_value, ff_parameter_value2, data, initial)+"/"+data['ENERGY_FILE_NAME']
    else:
        path= path_to_energy_folder_rerun(ff_parameter_value, ff_parameter_value2, data, initial)+"/"+data['ENERGY_FILE_NAME']
    print('The file is:', path)
    return path

#---------------------------------------------------
#---------------------------------------------------

def path_to_energy_folder_rerun(ff_list_value, ff_list_value2, data, initial=True):           # Have to change them to include only data-frame reading
    """
    Returns the full path to the energy folder f the ff parameter: ff_parameter_value
    Initially implemented for the initial simulations
    Need to be extended for the case where initial=False/ Need other functions to handle the creation of new folders
    """
    path=""
    param1=list2list(ff_list_value, data)
    param2=list2list(ff_list_value2, data)

    ff = param1 + param2
    # Write a defination that generate a list 'ff' that contains name of the columns 

    save_dict, df_rerun, df_sim = Bio.open_pickle(data)
    print('param1 & param2', param1+param2)
    index_rerun = check_rerun(param1, param2, df_rerun, data)      # list
    Bio.close_pickle(save_dict, df_rerun, df_sim, data) 
    
    print('param',  param1 + param2)
    if len(index_rerun)==0:
        print('rerun does not exists !')
        # Suggest new folder name
        path, folder_name = suggest_rerun_folder(data)
        print('ff & folder_name', ff, folder_name)
        save_dict, df_rerun, df_sim = Bio.open_pickle(data)
        mbis.rerun_energies(path, ff_list_value, ff_list_value2, data)
        df_rerun.loc[folder_name] = ff
        Bio.close_pickle(save_dict, df_rerun, df_sim, data)
        #Bio.save_rerun_df(int(folder_name), param1, param2)                # should we save to the data frame now or later??
        return path                 # Here, path is the path of the folder itself

    elif len(index_rerun)==1:
        print('rerun exists!')
        [path] = index_rerun         # Here 'path' is name of the folder.
        return data['ROOT_DIR']+data['RERUN_FOLDER']+'/'+str(path)+'/'

    else:
        print('----')
        print('It is possible that there are many rerun entries in the dataFrame.')
        print('We will take the first entry.')
        print('----')
        path = index_rerun[0]
        return data['ROOT_DIR']+data['RERUN_FOLDER']+'/'+str(path)+'/'    # Here, path is name of the folder
        

def check_rerun(param1, param2, df, data):
    ff=param1+param2
    print('ff', ff)
    if len(ff)==2:
        index = df.index[(df['ff1']==ff[0]) & (df['ff_rerun1']==ff[1])].tolist()
    elif len(ff)==4:
        index = df.index[(df['ff1']==ff[0]) & (df['ff2']==ff[1]) & (df['ff_rerun1']==ff[2]) & (df['ff_rerun2']==ff[3])].tolist()
    return index 

def check_sim(param, df, data, synthetic=False):
    #synthetic=True
    if not synthetic:
        if len(param)==1:
            index = df.index[(df['ff1']==param[0])].tolist()
        elif len(param)==2:
            index = df.index[(df['ff1']==param[0]) & (df['ff2'] == param[1])].tolist()
        print('check_sim - Index, ', index)
        
    if synthetic:
        df = Bio.open_synthetic_df(data)
        if len(param)==1:
            index = df.index[(df['ff1']==param[0])].tolist()
        elif len(param)==2:
            index = df.index[(df['ff1']==param[0]) & (df['ff2'] == param[1])].tolist()
        print('check_sim - synthetic - Index, ', index)
        
    return index

def suggest_rerun_folder(data):                     # Try it more tidiously
    """
    Output: 
        path_name  ->   string
        n          ->   interger
    """
    path_name=data['ROOT_DIR']+data['RERUN_FOLDER']+'0/'
    n = 0
    while os.path.exists(path_name)==True:
        n+=1
        path_name=str(data['ROOT_DIR']+data['RERUN_FOLDER']+'{}/'.format(n))          
    return path_name, n 

def suggest_sim_folder(data):                     # Try it more tidiously
    path_name=data['ROOT_DIR']+data['PROJECT']+data['INITIAL_SIMULATIONS']+'0/'
    n = 0
    while os.path.exists(path_name)==True:
        n+=1
        path_name=str(data['ROOT_DIR']+data['PROJECT']+data['INITIAL_SIMULATIONS']+'{}/'.format(n))  
    return path_name, n


## Bookkeeping: Folder creation and organization
#---------------------------------------------------
#---------------------------------------------------
def copyFiles(origin, destiny):
    """
    Copy the files located in origin to the destiny folder
    Args:
        complete_path (string): path to the folder
    """
    # more functionality could go into this function 
    if os.path.exists(origin)==False or os.path.exists(destiny)==False:
        print("Folders do not exist!!!")
    else:
        os.system("cp -r "+ origin+"* "+destiny)
        
#---------------------------------------------------
#---------------------------------------------------        
def create_folder(complete_path):
    """
    Creates directory with the complete_path
    
    Args:
        complete_path (string): path to the folder
    """
    # more functionality could go into this function 
    if os.path.exists(complete_path):
        print("Folder exists: ", complete_path)
        return -1
    else:
        os.mkdir(complete_path)
        return 0
    
#---------------------------------------------------
#---------------------------------------------------        
def rm_files(complete_path, files):
    """
    Creates directory with the complete_path
    
    Args:
        complete_path (string): path to the folder
    """
    starting_folder=os.getcwd()
   
    
    if os.path.exists(complete_path)==False:
        print("Folder do not exist!!!")
    else:
        os.chdir(complete_path)
        os.system("rm "+ files)
    
    os.chdir(starting_folder)

#---------------------------------------------------
#---------------------------------------------------        
def append_str_list(float_list, decimal=4):
    """ Converts float items in given list to strings and appends a new list with new string items
    
    Args:
        float_list(list): List of floats
        
    Returns:
        str(list): List of strings
    """
    str_list = []
    for i in float_list:
         str_list.append(to_str(i, decimal))
    return str_list

#---------------------------------------------------
#---------------------------------------------------  
def to_str(param, decimal=4):
    """
    To string with the indicated number of decimal places
    """
    return ('{:.%sf}' %decimal).format(param)

#---------------------------------------------------
#--------------------------------------------------- 
def create_copy_files_rerun(ff_param1, ff_param2, path, data):
    """
    Creates the folders and copy the files for energy rerun
    This methods creates the folder in the RERUN_FOLDER (see constants definitions).
    
    Args:
        ff_param1, (float64): Value of the forcefield parameter for which a trajectory already exists.
        ff_param2, (float64): Value of the forcefield parameter for which we want to rerun the trajectory.
    """ 
    res=0
    #create the folder
    res=create_folder(path)
    if res!=-1:
        copyFiles(data['FILES_TO_RERUN'], path)
    
    return res
        
#---------------------------------------------------
#---------------------Manipulate simulation files ------------------------------ 
  
def create_itp_file_new_ff_parameter(path, ff_param2, data, verbose=False, output_verbose='output.txt'):
    """
    Creates a new .itp file in the given path (path) with the new ff parameter ff_param2.
    This methods only works for martini2 and it is relies on the script:scale_martini.py by Soeren von Buellow
    Args:
    path (string) : Path where the new .itp file will be created
    ff_param2 (list): This is an scaling factor for the vdW forces, as implemented in scale_martini.py. 
    """
    os.chdir(path)
    func_for_scaling_itp = Bio.read_function_for_scaling_itp(data)
    name_new_itp = func_for_scaling_itp(path, ff_param2)
    
    if os.path.exists(name_new_itp)==False:
        print("There was a problem creating the new itp file. Chech here: {}".format(path))
    return name_new_itp

#----------------------------------------
#----------------------------------------
def replace_line_topol_file( new_line, topol_name='topol.top', line_to_replace=0):
    """
    Replace one line of the topology file.
    Args:
    new_line (str): New line
    topol_name (str): name of the topology file. Default:'topol.top', 
    line_to_replace (int): Number of the line in the topololy file to be replaced. Default 0
    new_line (str): New line
    """
    # Opens the file and copy line by line. It also changes the first line to include the new .itp file
    topol_top = open(topol_name, "r") 
    string_list = topol_top.readlines() 
    string_list[line_to_replace]= new_line 
    topol_top.close()
    # Replace all the lines in the new topol.top file including the new .itp file
    topol_top = open(topol_name, "w")
    new_topol_top = "".join(string_list)
    topol_top.write(new_topol_top)
    topol_top.close()

#----------------------------------------
#----------------------------------------    
def exist_simulation(ff_param, simulations):
    """
    Check if a simulation with the given ff_param exist in the dataset
    """
    exist=False
    if ff_param in simulations:
        exist=True
    return exist
