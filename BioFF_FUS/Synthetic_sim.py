"""
Sergio Cruz-Leon, MPI of Biophysics, Frankfurt am Main, 2022

This file contains all the functions to create a synthetic trajectory using MBAR. The structures are resapled with the the reweighted energies for a force fiel parameter, given two know close simulations. 
It further explites the inverse tranform sampling to draw a list of observables with statistical weights 'w'.

This class requires the package pymbar from the Chodera lab:  https://pymbar.readthedocs.io/en/master/mbar.html
"""
######### IMPORT ################################
#################################################
import MDAnalysis
import os
import pandas as pd
import time
import numpy as np

# Funtions to do bookkeeping
from . import Bookeeping as bk
# constants
# gmx functions
from . import GMX_functions as gmxf
# Functions to calculate observables
#from . import Observables as obs
from . import BioFF as Bio
from . import MBAR_and_inverseSampling as mbis

#################################################
######### FUNCTIONS ##############################
#################################################

def create_synthetic_trajectory(
        ff_param_list, 
        data, 
        idx_ran_ener, 
        output_folder,  
        output_xtc_centered="md_pbcmol_centered.xtc", 
        verbose=False, 
        output_verbose='output.txt'): 
    """
    Args:
    ff_param1
    ff_param2, 
    ff_param_new, 
    idx_ran_ener, 
    xtc_name= (str). Default: c.XTC_NAME, 
    output_xtc (str).  Default: "synthetic.xtc"
    output_xtc_centered (str).  Default: 'md_pbcmol_centered.xtc'
    """
    xtc_name = data['XTC_NAME'] 

    print('create_synthetic_trajectory, ff_param_list - ', ff_param_list)
    # Path to the xtc trjaectory of the simulation with the parameter ff_param1 
    xtc_ff_param1=bk.path_to_simulation_files(ff_param_list[0], data)+xtc_name 
    # Path to the xtc trjaectory of the simulation with the parameter ff_param1
    xtc_ff_param2=bk.path_to_simulation_files(ff_param_list[1], data)+xtc_name 
    # Path to the xtc trjaectory of the simulation with the parameter ff_param1
    xtc_ff_param3=bk.path_to_simulation_files(ff_param_list[2], data)+xtc_name 
    # Path to the xtc trjaectory of the simulation with the parameter ff_param1
    xtc_ff_param4=bk.path_to_simulation_files(ff_param_list[3], data)+xtc_name 
    # Full path to the output synthetic traj - for now I am saving them in energy_folder_rerun ff_param2, ff_param_new
    #xtc_output = bk.path_to_energy_folder_rerun(ff_param2, ff_param_new)+"/"+output_xtc
    xtc_output = output_folder+"/"+xtc_name 

    #os.system(f"mkdir {output_folder}")
    #Create a synthetic trajectory using the frames of the simulations ff_param1 and ff_param2 and the weights from MBAR and inverse_transformation
    write_synthetic_xtc(data, xtc_ff_param1, xtc_ff_param2, xtc_ff_param3, xtc_ff_param4, idx_ran_ener, xtc_output)

    #path=bk.path_to_energy_folder_rerun(ff_param2, ff_param_new)
    path=output_folder 
    os.chdir(output_folder) 

    # Unwrap the trajectory to correct for PBC and center the protein
    gmxf.load_gmx_modules(data) 
    gmxf.gmx_unwrap_xtc(data, verbose=verbose, output_verbose=output_verbose)
    
    os.system("cp {} {}".format(data['INIAL_GRO_FILE_NOWATER'], path))
    

def  write_synthetic_xtc(data, xtc_ff_param1, xtc_ff_param2, xtc_ff_param3, xtc_ff_param4, idx_ran_ener, xtc_output):
    """
    initial_gro_file (str); Full path to the initial .gro file used to create the trajectory. Default: INITIAL_GRO_FILE
    xtc_ff_param1: Full path to the existent trajectory (.xvg file) for the ff parameter 1
    xtc_ff_param1: Full path to the existent trajectory (.xvg file) for the ff parameter 2
    idx_ran_ener: Indices ofthe frames that satisfy the weighted energies. Output from inverse_transform_sampling
    xtc_output: synthetic trajectory for the new ff_parameter (xtc file)
    """
    initial_gro_file = data['INITIAL_GRO_FILE']
    try:
        u = MDAnalysis.Universe(initial_gro_file, xtc_ff_param1, xtc_ff_param2, xtc_ff_param3, xtc_ff_param4)
        #protein = u.select_atoms("protein")
        protein = u.select_atoms("all")
        with MDAnalysis.Writer(xtc_output, protein.n_atoms) as W:
            for idx in idx_ran_ener:
                u.trajectory[idx]
                #print("old", u.trajectory[idx])
                W.write(protein)
        print("Trajectory should be here: {}".format(xtc_output))        
    except:
        print('Problem writng the trajectory')

#----------------------------------------
#----------------------------------------
def find_param_above(ff_param, df_sim, idx):
    """
    Finds the existing simulation with the closest parameter above to ff_param
        
    Parameters
       -  ff_param (float)    : force-field parameter for which we try to find the closest existing simulation above
       -  df_sim (DataFrame)  : DataFrame that contains all the force field parameters for which the simulation exists.
       -  idx (int)           : column of the DataFrame that we need to access from the DF.
    
    Return 
    (float) Parameter above for which there is a simulation already.
    """
    listFF=df_sim[f'ff{int(idx+1)}'].tolist()
    try:
        above = min([i for i in listFF if float(ff_param[idx]) < float(i)])
    except:
        print('There is no simulation above. You should consider to run a new simulation. \n Meanwhile we take the value below')
        above = max([i for i in listFF if float(ff_param[idx]) > float(i)])
    return above

#----------------------------------------
#----------------------------------------
def find_param_below(ff_param, df_sim, idx):
    """
    Finds the existing simulation with the closest parameter below to ff_param
        
    Parameters
       -  ff_param (float)    : force-field parameter for which we try to find the closest existing simulation above
       -  df_sim (DataFrame)  : DataFrame that contains all the force field parameters for which the simulation exists.
       -  idx (int)           : column of the DataFrame that we need to access from the DF.
    
    Return 
    (float) Parameter below for which there is a simulation already.
    """
    print('find param below: -')
    print('ff_param, ', ff_param)
    print('df_sim, ', df_sim)
    print('idx, ', idx)
    listFF=df_sim[f'ff{int(idx+1)}'].tolist()
    try:
        below = max([i for i in listFF if float(ff_param[idx]) > float(i)])
    except:
        print('There is no simulation below. You should consider to run a new simulation. \n Meanwhile we take the value above')
        below = max([i for i in listFF if float(ff_param[idx]) < float(i)])
    return below

#----------------------------------------
#----------------------------------------
def recalculate_energy_for_synthetic_sim(ff_param, data, folder_path):
    """
    Creates a new synthetic simulation including all the files. 
    We need to prepare files for mbar and run it.
    Input: 
        - ff_param (list of list) : The forcefield parameter where we need a synthetic simulation
        - data (dict) : contains all the information about the BioFF initial parameters.
    Output: 
        - (None) Creates a sythetic simulation
    """
    # Check if the simulation exists?
    #folder_path=bk.path_to_simulation_files(ff_param, data)
    if not os.path.exists(folder_path):
        junk, df_rerun, df_sim = Bio.open_pickle(data)
        forceField = df_sim.values[:100]
        list_upper_lower = find_neighbours(ff_param, forceField)
        list_neighbours = assemble_neighbours(list_upper_lower)
        
        nframes = int(data['NFRAMES'])
        N_k = [nframes, nframes, nframes, nframes, 0]
        print('createSyntheticSimulation - N_k', N_k)

        ff_parameter_list = list_neighbours + [ff_param]
        print('createSyntheticSimulation - ff_parameter_list, ', ff_parameter_list)
        u_kn= mbis.apply_mbar_get_energies(ff_parameter_list , N_k, data)
        print('-'*25)
        print('apply_mbar_get_enegies --- completed ----')
        print('-'*25)
        weights = mbis.mbar_get_weights(u_kn, data['T'], data['K_BOLTZMANN'] , N_k)
        print('----------------------------- -------------------------')
        print('-------Inverse sampling' '------')
        print('----------------------------- -------------------------')
        # From MBAR we got back the energies and the weights
        x_energy = u_kn[4]
        w = weights[4]
        print('recalculate_energy_for_synthetic_simulation - weight', w, w.shape)
        x_new_ener, idx_ener, idx_ran_energ = mbis.inverse_transform_sampling(x_energy, w, nframes)
        return idx_ran_energ, ff_parameter_list, folder_path
        
    else:
        print('='*25)
        print(f'The simulation exists at the ff-value {ff_param},\n Synthetic simulation will not be created')
        print('='*25)
        return -1, ff_param, folder_path
    

    
def assemble_neighbours(list_upper_lower):
    list_neighbours = []
    for item in list_upper_lower[0]:
        for item2 in list_upper_lower[1]:
            list_neighbours.append([item, item2])
    return list_neighbours
    
def separate(forceField):
    numFF_param = len(forceField[0])
    sepBins = [[] for k in range(numFF_param)]
    for j in range(len(forceField)):
        for i in range(numFF_param):
            sepBins[i].append(forceField[j][i])
    return sepBins

def find_neighbours(ff_param, forceField):
    numFF_param = len(forceField[0])
    sepBins = np.array(separate(forceField), dtype='float64')
    ff_param = np.array(ff_param, dtype='float64')
    axes = [[] for k in range(numFF_param)]
    for i in range(numFF_param):
        #axes[i].append(sepBins[i][sepBins[i]<ff_param[i]].max() )  # smaller values 
        #axes[i].append(sepBins[i][sepBins[i]>ff_param[i]].min() )  # higher values
        try:
            axes[i].append(sepBins[i][sepBins[i]<ff_param[i]].max() )
        except ValueError as ve:
            axes[i].append(sepBins[i][sepBins[i]>ff_param[i]].min() )
            
        try:
            axes[i].append(sepBins[i][sepBins[i]>ff_param[i]].min() )
        except ValueError as ve:
            axes[i].append(sepBins[i][sepBins[i]<ff_param[i]].max() )  # higher values
            
            
    return axes


def create_synthetic_simulation(ff_parameter_list, idx_ran_energ, data, folder_path, verbose=False, output_verbose='output.txt'):
    """
    Creates a new synthetic simulation including all the files.
    It assummes that MBAR was runned already
    ff_parameter_list (list): [param_below, find_param_above, param_new]
    idx_ran_energ (list): Output from inverse sampling method. Indices on the sorted energies
    """
    # Load names 
    xtc_name=data['XTC_NAME'] ; output_xtc=data['XTC_NAME'] ; output_xtc_centered='md_pbcmol_centered.xtc'
    # List of parameters to rerun the energies
    print('ff_parameter_list', ff_parameter_list)
    #folder_path=bk.path_to_simulation_files(ff_parameter_list)
    gmxf.load_gmx_modules(data)
    if  os.path.exists(folder_path):
        
        print('THERE IS A SIMULATION ALREADY FOR THE GIVEN VALUE!!!')
        
    else:
        print('='*24)
        print('create a synthetic simulation at {}'.format(folder_path))
        print('='*24)
        bk.create_folder(folder_path)

        # copy all the files to the new folder
        origin_files=data['FILES_TO_RERUN']
        bk.copyFiles(origin_files, folder_path)

        #Create the new itp file
        new_itp=bk.create_itp_file_new_ff_parameter(folder_path, ff_parameter_list[4], data)
        new_line='#include "'+new_itp+'"\n'

        #modify the topo.top file
        bk.replace_line_topol_file(new_line, topol_name='topol.top', line_to_replace=0)

        #Run grompp to create the new .tpr file
        gmxf.gmx_grompp(data, verbose=verbose, output_verbose=output_verbose)


        # Remove redundant files
        #bk.rm_files(folder_path, "md.xtc")
        #bk.rm_files(folder_path, "md.log")
        #bk.rm_files(folder_path, "mdout.mdp")
        #bk.rm_files(folder_path, "md.xvg")
        #bk.rm_files(folder_path, "output.txt")
        #bk.rm_files(folder_path, "ener.edr")

        if verbose:
            print('----------------------------- -------------------------')
            print('-------Creating a synthetic trajectory for the value {}-----------------'.format(ff_parameter_list[2]))
            print('-------All the files are located in {}-----------------'.format(folder_path))
            print('----------------------------- -------------------------')
            #
        create_synthetic_trajectory(ff_parameter_list, data, idx_ran_ener=idx_ran_energ, output_folder=folder_path,
                                            output_xtc_centered=output_xtc_centered, verbose=False, output_verbose='output.txt')

        # Move to the directory:
        os.chdir(folder_path)

        #Rerun 
        gmxf.gmx_mdrun_rerun(data)
        ## Rerun the energy:
        gmxf.gmx_energy(data)

        
#def run_new_simulation(force_field_new, deffnm='md', mdp_file='md.mdp', output_xtc=c.XTC_NAME, output_xtc_centered="md_pbcmol_centered.xtc",  const_file='npt_12ns.gro', topol_name='topol.top', tpr_file='md.tpr', output_verbose='output.txt', verbose=False,  openMPI=True):
#    """
#    This function creates a new simulation
#    """
#        # Path to create the new simulation
#    
#    
#    folder_path=bk.path_to_simulation_files(force_field_new, data)
#    print (folder_path)
#    if  os.path.exists(folder_path):
#        print('THERE IS A SIMULATION ALREADY FOR THE GIVEN VALUE!!!')
#    else:
#        origin_files=bk.create_folder(folder_path)
#        # copy all the files to the new folder
#        bk.copyFiles(c.FILES_TO_RERUN, folder_path)
#
#        gmx=gmxf.load_gmx_modules(c.GMX_MODULE)
#
#        #Create the new itp file
#        new_itp=bk.create_itp_file_new_ff_parameter(folder_path, force_field_new)
#        new_line='#include "'+new_itp+'"\n'
#
#        #modify the topo.top file
#        bk.replace_line_topol_file(new_line, topol_name='topol.top', line_to_replace=0)
#        print ('Running grompp')
#        #Run grompp to create the new .tpr file
#        gmxf.gmx_grompp(gmx=c.GMX, mdp_file=mdp_file, const_file=const_file, topol_file=topol_name, output_file=tpr_file, verbose=verbose, output_verbose=output_verbose)
#
#        ## @Sergio: Here, I could submit a slurm job and wait for it to finish.
#        gmxf.gmx_mdrun(gmx=c.GMX_MPI, deffnm=deffnm, tpr_file=tpr_file, verbose=verbose, openMPI=openMPI)
#        
#
#
#
#    ## @Sergio: Now, I need to analyze the trajectory. But this should be done in a separate function to allow the user to do wherever analysis he/she wants.
#    ### POSTPROCESSING OF THE TRAJECTORY    
#    # Unwrap the trajectory to correct for PBC and center the protein
#    xtc_output = folder_path+"/"+output_xtc
#    
#    gmxf.load_gmx_modules()
#    gmxf.gmx_unwrap_xtc(gmx=c.GMX, xtc_file=output_xtc, tpr_file=tpr_file, output=output_xtc_centered, verbose=verbose, output_verbose=output_verbose)
#
#    # Copy this file to calculate the Rg - This is of course only true for the current observable.
#    os.system("cp {} {}".format(c.INIAL_GRO_FILE_NOWATER, folder_path))
#
#    ## Calculate Rg ->
#    rg_alpha_new_new = obs.rad_gyration(bk.path_to_simulation_files(force_field_new, data))
    
    

    
####### 27.10.2022

def prepare_new_simulation(force_field_new, data, output_verbose='output.txt', verbose=False,  openMPI=True):
    """
    This function prepares to run a simulation with the desired force-field value. 
    It creates the necessary folders and files, as well as the force-field changes.
    -------------
    Parameters:
    force_field_new= (float) new value of the ff_parameter for which we want to prepare a simulation, 
    data= (dict) contains the BioFF initialization parameter
    output_verbose='output.txt', 
    verbose=False,  
    openMPI=True
    -------------
    Return 
    force_field_new, 
    mdp_file, 
    const_file, 
    topol_name
    """
    # Path to create the new simulation
    deffnm = 'md' ; mdp_file='md.mdp' ; output_xtc=data['XTC_NAME'] ; output_xtc_centered = 'md_pbcmol_centered.xtc' ; const_file='npt_12ns.gro'
    topol_name = 'topol.top' ; tpr_file = 'md.tpr'
    
    folder_path=bk.path_to_simulation_files(force_field_new, data)
    print (folder_path)
    if  os.path.exists(folder_path):
        print('THERE IS A SIMULATION ALREADY FOR THE GIVEN VALUE!!!')
    else:
        origin_files=bk.create_folder(folder_path)
        # copy all the files to the new folder
        bk.copyFiles(data['FILES_TO_RERUN'], folder_path)

        gmx=gmxf.load_gmx_modules(data)

        #Create the new itp file
        new_itp=bk.create_itp_file_new_ff_parameter(folder_path, force_field_new, data)
        
        #modify the topo.top file
        new_line='#include "'+new_itp+'"\n'
        bk.replace_line_topol_file(new_line, topol_name='topol.top', line_to_replace=0)
        print ('Running grompp')
        #Run grompp to create the new .tpr file
        gmxf.gmx_grompp(data, verbose=verbose, output_verbose=output_verbose)
        
        
        return force_field_new, mdp_file, const_file, topol_name
        

def recalculate_energy_for_synthetic_sim1111(ff_parameter_value_new, data):
    """
    @Sergio: This should be splitted in several functions
    Recalculate energies for a given ff_parameter_value_new.
    
        - Takes the closest existing simulations from above and from below. Create a vector [below_ff, above_ff, new_ff]
        - Rerun energies for: 
            - Check if the energies have been reruned already, if not calcultes:
                -[below_ff,  new_ff]
                -[above_ff,  new_ff]
                -[above_ff,  below_ff]
                -[below_ff,  above_ff]
    
    Return a single one-dimensional array containing the new energies of all simulations.
    
    Args:
    
        alpha (float): The current scaling parameter of the LJ pair-pair interaction.
        alphas2Bopt (float or list): List of alpha values includes previous alpha scaling parameters: alpha_ref and BioFF values.
        path_diff_test (string): Energy rerun folder for alpha_star with alpha1 and alpha 2
        init_sim_path (string): Full path to initial simulations
        
    Return:
        all_u (array): All energies for the current value of alpha
    """
    #  Check if the simulations exists in the df from the pickle for such simulations.
    ff_param=[]
    try:
        junk, df_rerun, df_sim = Bio.open_pickle(data)
        #ff_param = ['1.0000', '1.0000']
        for idx in df_sim.index:
            item = df_sim.loc[[idx]]
            force_field = []
            for col in item:
                force_field.append(item[col].values[0])
            
            if force_field == bk.list2list(ff_parameter_value_new):
                print("--------------------------------------------------------------------------------------")
                print(f"----- There is a simulation already for the ff_param value {item.to_string(index=False, header=False)} -----")
                print("-----               A synthetic simulation was not created!!                    ------")
                print("--------------------------------------------------------------------------------------")
                return -1, ff_parameter_value_new
    except:
        print('----------------------------------------------------------------------------------------------')
        print(f'Check if your pickle file exists {c.ROOT_DIR+c.PROJECT+c.PICKLE_FILE} -----------------')
        print(f'Else check if the pickle file has some entries for the df_sim -------------------------------')
        print("Either the pickle file or the list of forcefield parameters does not exist!!! check!")
        print("----------------------------------------------------------------------------------------------")
        
    junk, df_rerun, df_sim = Bio.open_pickle(data)      # Loads the pickle file automatically
    
    len_avail = len(bk.check_sim(bk.list2list(ff_parameter_value_new), df_sim, data))
    #Check if the energies have already been calculated if not, calculate them.
    print('recalculate energy for the syn, len_avail, ', len_avail)
    if len_avail == 0:
        ff_param_upper, ff_param_below = [[] for i in range(2)]
        for i in range(len(ff_parameter_value_new)):
            ff_param_below.append(find_param_below(ff_parameter_value_new, df_sim, i))
            ff_param_upper.append(find_param_above(ff_parameter_value_new, df_sim, i))

        #####  THIS IS THE PART THAT NEEDS TO BE UPDATED TO RUN MULTIPLE PARAMETER - BEYOND 2D  ####
        # hash map the force field parameters above and below. List of parameters to rerun the energies
        ff_parameter_list = [
            ff_param_below,               # One of the points
            [ ff_param_below[0], ff_param_upper[1] ],     # rearrange to get more points
            [ ff_param_upper[1], ff_param_below[0] ],     # rearrange to get more points
            ff_param_upper,                # one of the points
            ff_parameter_value_new
        ]
        
        # List of parameters to rerun the energies
        #ff_parameter_list = [ff_parameter_value1, ff_parameter_value2, ff_parameter_value_new]
        
        #Concatenate the lists
        ff_param += ff_parameter_list
        print('recal-- ff_parameter_list, ', ff_parameter_list)
        print('recal-- ff_param', ff_param)
        
        print(' --- CREATING A SYNTHETIC SIMULATION--------------')
        print(' --- Re-runing the energies for MBAR --------------')
        starting_folder=c.ROOT_DIR
        start_time = time.time()
        
        print("---Time for rerun energies %s seconds ---" % (time.time() - start_time))
        os.chdir(starting_folder)
        
        print('----------------------------- ----------------------------------------------------')
        print('-------Running MBAR for a new ff parameter', ff_parameter_value_new, '------------')
        print('-------There are simulations already for', ff_parameter_list, '---------')
        print('----------------------------------------------------------------------------------')
        
        ###### CORRECTED UNTIL HERE ######
        
        frame_number=c.NFRAMES

        #The next two lines are used for mbar. They indicate the number of energy frames for each force field parameter.
        #[ff_parameter_value1 (1001 frames), ff_parameter_value2 (1001 frames), ff_parameter_value_new (0 frames)]
        #N3 = 0
        N_k = [1001, 1001, 1001, 1001, 0]
        
        u_kn= mbis.apply_mbar_get_energies(ff_parameter_list , N_k, data)
        
        # --------- Build Number of frames ----------
        #number_of_k_struc = [1001, 1001, 1001, 1001, 0]    # AUTOMATE
        #N_k = []
        #for idx in number_of_k_struc[:-1]:
        #    num_k_struc = []
        #    for idx2 in number_of_k_struc:
        #        num_k_struc.append(idx2)
        #    N_k.append(num_k_struc)
        # -------------------------------------------f
        
        weights = mbis.mbar_get_weights(u_kn, c.T, c.K_BOLTZMANN , N_k)
        
        print('----------------------------- -------------------------')
        print('-----------------   Inverse sampling    ---------------')
        print('----------------------------- -------------------------')
        # From MBAR we got back the energies and the weights
        x_energy = u_kn[4]
        w=weights[4]
        x_new_ener, idx_ener, idx_ran_energ = mbis.inverse_transform_sampling(x_energy, w, frame_number)
    
    else:
        print('Should have some simulations, prior testing is implemented')
    
    return idx_ran_energ, ff_param