"""
Sergio Cruz-Leon, Harika Urel and Juergen Koefinger, MPI of Biophysics, Frankfurt am Main, 2022


Utility functions to manipulate simulation files with Gromacs functions. It only serves as an interface between python and gromacs.

This class requires that gromacs in previously installed and the path is defined in the variable GMX. 
"""
#################################################
######### IMPORT ################################
#################################################

import os
import os.path
import time
import subprocess


#################################################
#########Functions ##############################
#################################################

#-------------------------------------------
#-------------------------------------------

#-------------------------------------------
#-------------------------------------------
def gmx_grompp(data, verbose=False, output_verbose='output.txt'):  # verbose?
    """
    Runs gmx grompp with the given parameters, see gromacs manual (https://manual.gromacs.org/documentation/2021/manual-2021.pdf).

    Args: 
        - data (dict) : Contains parameters used for BioFF initialization. 
        - verbose (bool) 
        - output_verbose (txt) : Name of output file in case of verbose.

    Returns:  
        - None
    """
    gmx = data['GMX']; mdp_file=data['MDP_FILE']; const_file=data['INITIAL_GRO_FILE']; topol_file=data['TOPOL_FILE']; output_file=data['TPR_NAME']
    if verbose:
        print(f'{gmx} grompp -f {mdp_file} -c {const_file} -p {topol_file} -o {output_file} -nobackup')
        os.system(f'{gmx} grompp -f {mdp_file} -c {const_file} -p {topol_file} -o {output_file} -maxwarn 1  -nobackup')
    else:
        print(f'{gmx} grompp -f {mdp_file} -c {const_file} -p {topol_file} -o {output_file} -nobackup 2>> {output_verbose}')
        os.system(f'{gmx} grompp -f {mdp_file} -c {const_file} -p {topol_file} -o {output_file} -maxwarn 1 -nobackup 2>> {output_verbose}')
    #p = subprocess.Popen(
    #        [
    #        ], 
    #        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #out, err = p.communicate()  
    #p.wait()
    return 
        
#-------------------------------------------
#-------------------------------------------
def load_gmx_modules(data):
    """
    Loads gromacs module
    Returns GMX_executable, required for running the simulations
    Args:
        - data (Dict) : Contains parameter used for BioFF initialization.
    
    Returns:
        - None    
    """
    module=data['GMX_MODULE']
    try:
        subprocess.run(f"module load {data['GMX_MODULE']}",shell=True)
        #os.system('module load {}'.format(c.GMX_MODULE))
    except:
        print("The module {} does cannot be loaded, please check!".format(module))
    
    if module==data['GMX_MODULE']:
        return data['GMX']

    else:
        print('Please change the value of the constant GMX in the file constants.py')

#-------------------------------------------
#-------------------------------------------
def gmx_mdrun_rerun(data, verbose=False, output_verbose='output.txt'):
    """
    Runs gmx mdrun rerun with the given parameters. See gromacs manual (https://manual.gromacs.org/documentation/2021/manual-2021.pdf)

    Args: 
        - data (Dict) : Contains parameters used for BioFF initialization.
        - verbose (bool) : True or False
        - output_verbose (text) : Name of the output file in case of verbose.

    Returns: 
        - None
    """
    gmx=data['GMX']; tpr_file=data['TPR_NAME']; xtc_file=data['XTC_NAME']; ener_file=data['ENER_EDR']
    if verbose:
        os.system(f"{gmx} mdrun -s {tpr_file} -rerun {xtc_file} -e {ener_file} -rdd 1.8 -nt 8 -update cpu -nobackup ")
    else:
        os.system(f"{gmx} mdrun -s {tpr_file} -rerun {xtc_file} -e {ener_file} -rdd 1.8 -nt 8 -update cpu -nobackup  2>> {output_verbose} ")
        

#-------------------------------------------
#-------------------------------------------
def gmx_energy(data, verbose=False, output_verbose='output.txt'):
    """
    Runs gmx energy with given parameters. See gromacs manual (https://manual.gromacs.org/documentation/2021/manual-2021.pdf).

    Args: 
        - data (Dict) : Contains parameters used for BioFF initialization.
        - verbose (bool) : True or False
        - output_verbose (text) : Name of the output file in case of verbose.

    Returns: 
        - None
    """
    #retrieve the energies from rerun
    group=data['ENERGY_GROUP']; gmx=data['GMX']; ener_file=data['ENER_EDR']; output=data['ENERGY_FILE_NAME']
    if verbose:
        os.system(f"echo '{group}'|{gmx} energy -f {ener_file} -o {output} -nobackup")
    else:
        os.system(f"echo '{group}'|{gmx} energy -f {ener_file} -o {output} -nobackup 2>> {output_verbose} ") 
        
#-------------------------------------------
#-------------------------------------------
def gmx_unwrap_xtc (data, verbose=False, output_verbose='output.txt'):
    """
    Runs gmx trjconv to unwrap the trajectory file to center and fit periodic boundary conditions. 
    See gromacs manual (https://manual.gromacs.org/documentation/2021/manual-2021.pdf).
    
    Args: 
        - data (Dict) : Contains parameters used for BioFF initialization.
        - verbose (bool) : True or False
        - output_verbose (text) : Name of the output file in case of verbose.

    Returns: 
        - None
    """
    gmx=data['GMX']; xtc_file=data['XTC_NAME']; tpr_file=data['TPR_NAME']; output=data['OUTPUT_SYNTHETIC_TRAJECTORY']
    format_echo = "1 1"
    if verbose:
        os.system("echo '{}'|{} trjconv -f {} -s {} -o {} -pbc mol -center".format(format_echo, gmx, xtc_file, tpr_file, output ))
    else:
        os.system("echo '{}'|{} trjconv -f {} -s {} -o {} -pbc mol -center 2>> {} ".format(format_echo, gmx, xtc_file, tpr_file, output, output_verbose ))

#-------------------------------------------
#-------------------------------------------
def gmx_export_first_frame(data, beg=0, end=500000, verbose=False, output_verbose='output.txt'):
    """
    Runs gmx trjconv to unwrap the trajectory file to center and fit periodic boundary conditions.  
    See gromacs manual (https://manual.gromacs.org/documentation/2021/manual-2021.pdf).
    
    Args: 
        - data (Dict) : Contains parameters used for BioFF initialization.
        - verbose (bool) : True or False
        - output_verbose (text) : Name of the output file in case of verbose.

    Returns: 
        - None
    """
    gmx=data['GMX']; xtc_file=data['OUTPUT_SYNTHETIC_TRAJECTORY']; tpr_file=data['TPR_NAME']; output='md_pbcmol_centered.gro'; 
    format_echo = "1 "
    if verbose:
        os.system("echo '{}'|{} trjconv -f {} -s {} -o {} -b {} -e {}".format(format_echo, gmx, xtc_file, tpr_file, output, str(beg), str(end)))
    else:
        os.system("echo '{}'|{} trjconv -f {} -s {} -o {} -b {} -e {} 2>> {}".format(format_echo, gmx, xtc_file, tpr_file, output, str(beg), str(end), output_verbose))
                   
#-------------------------------------------
#-------------------------------------------
def gmx_mdrun(data, verbose=False, output_verbose='output.txt', openMPI=False ):
    """
    Runs gmx mdrun rerun with the given parameters. 
    See gromacs manual (https://manual.gromacs.org/documentation/2021/manual-2021.pdf).
    
    Args: 
        - data (Dict) : Contains parameters used for BioFF initialization.
        - verbose (bool) : True or False
        - output_verbose (text) : Name of the output file in case of verbose.
        - openMPI (bool) : MPI compilation available for running simulation on multiple nodes.
        
    Returns: 
        - None    
    """
    deffnm='md'; xtc_file=data['XTC_NAME']; tpr_file=data['TPR_NAME']; ener_file=data['ENER_EDR'] 
    if openMPI:
        gmx=data['GMX_MPI']
         
        if verbose:
            os.system(" {} mdrun -deffnm {}  -x {} -s {} -e {} -nobackup  -v  -bonded gpu -nb gpu -update gpu ".format(gmx, deffnm, xtc_file, tpr_file, ener_file ))
        else:
            os.system(" {} mdrun -deffnm {} -x {} -s {} -e {} -nobackup  -v  -bonded gpu -nb gpu -update gpu  2>>{} ".format(gmx, deffnm, xtc_file, tpr_file, ener_file, output_verbose))
        
    else:
        gmx=data['GMX']
        if verbose:
            os.system(" {} mdrun -deffnm {}  -x {} -s {} -e {} -nobackup  -v   ".format(gmx, deffnm, xtc_file, tpr_file, ener_file ))
        else:
            os.system(" {} mdrun -deffnm {} -x {} -s {} -e {} -nobackup  -v  2>>{} ".format(gmx, deffnm, xtc_file, tpr_file, ener_file, output_verbose))
    
