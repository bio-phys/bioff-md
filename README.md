# BioFF - Scaling of protein-protein interactions - FUS Martini 2

This repository uses Bayesian inference of force fields to reproduce experimental properties of FUS in the force-field Martini 2. 
We use the method BioFF [[1]](https://link.springer.com/article/10.1140/epjb/s10051-021-00234-4) to infer the scaling factor `alpha` for protein-protein interactions using experimental properties of FUS [[2]](https://pubs.acs.org/doi/full/10.1021/acs.jctc.0c01064). 

## Description

## Requirements

    - anaconda3
	- python > 3.9
	- BioEn (https://github.com/bio-phys/BioEn)
	- Binless-WHAM (https://github.com/bio-phys/binless-wham)
	- pymbar (https://github.com/choderalab/pymbar)
	- asyncmd (https://gitea.kotspeicher.de/AIMMD/asyncmd). - It requires python > 3.9
	- gromacs

## Create a CONDA environment 

	- conda env create -f IDP_env.yaml  python=3.9
		
# Execute BioFF
## Finds iteratively the scaling factor `alpha`

![alt text](<./images/Iterations.png>) 


## Notes:
The scripts of this repository have been tested in the cluster [BIO](https://docs.mpcdf.mpg.de/doc/computing/clusters/systems/Biophysics/Biophysics-BIO.html) of the MPCDF. 

It requires the following modules:
- anaconda/3/2021.11   
- gromacs/2020  

Furthermore, the path to your gromacs executable needs to be known by your environment variable PATH
You need to include in yourr `~/.bashrc` file. Something like:

  `export GROMACS_HOME=/mpcdf/soft/SLE_15/packages/skylake/gromacs/gcc_8-8.4.0-impi_2019.6-2019.6.166/2019.6/`
  `PATH=${GROMACS_HOME}/bin:${PATH}`

or, try loading the gromacs and anaconda module that are available to your cluster, before starting any jupyter notebooks. 

## Setting the cluster variables

Modify the file `BioFF_FUS/constants.py` according to your cluster or computer paths (see an example in `constants_BIO.py`).


# Case 1: Synthetic simulation

'Synthetic simulation' is a method we implemented to speed up the iterations over BioFF. In the iteration procedure, BioFF infers optimal parameters for the force field parameters, and it needs to run an additional simulation for the newly optimized force field parameters. Here, we do not need to wait until a new simulation finishes to iterate over BioFF. Instead, we run -from the beginning-  a set of simulations on with different force field values on a grid. Then, at every BioFF iteration, we resample a "synthetic trajectory" very efficiently with the correct statistical weights of the desired new force field parameter. Therefore, we can calculate observables over the synthetic trajectory and continue the BioFF iteration. 

![alt text](<./images/Synthetic_simulations.png>) 

Consider the protein-protein scaling &alpha; [[2]](https://pubs.acs.org/doi/full/10.1021/acs.jctc.0c01064) as an example. For this case, we performed 21 simulations with &alpha; &isin; [0, 1] uniformly distributed. Let's assume we start the BioFF iteration, and the best-inferred parameter is &alpha;', which was not simulated initially. Therefore, to continue the optimization, we would require to run a new simulation for &alpha;'. Here, instead of running a new simulation, we sample a synthetic trajectory: (i) We select the closest force-field values above and below &alpha;' in the initial sampling grid. (ii) We use MBAR to obtain the statistical weights that each frame in the existing trajectories will have in the case of &alpha;'. (iii) The inverse sampling algorithm [[3]](https://en.wikipedia.org/wiki/Inverse_transform_sampling) allows us to draw frames from the existing trajectories with the correct statistical weights. By combining the frames, we end with a "synthetic trajectory" for &alpha;'. The new synthetic trajectory can be analyzed to extract the observables and continue BioFF iterations.  


# How to start?
0.  Clone the repository


	0.1	Modify the file `BioFF_FUS/constants.py` and modify the constants with your paths.


1.	Running concurrent simulations: Open and execute the notebook: `1_Concurrent_simulations.ipynb`. A short description on how to run jupyter notebooks in the clusters can be found on this [link](http://tbwiki.tb.biophys.mpg.de/index.php/Jupyter_notebooks) 

To run concurrent simulations, you need to prepare all the input files. See `examples/input_files_FUS` for the input files of FUS.

To submit the jobs to your preferred cluster, you need a SLURM master. An example prepared for [BIO](https://docs.mpcdf.mpg.de/doc/computing/clusters/systems/Biophysics/Biophysics-BIO.html) can be found in: `examples/Slurm`.


	1.1	 Run a set of simulations with asyncmd (serial)
	
	1.2	 Run a set of simulations -all at once- with asyncmd: 
         You need this step for the next part (synthetic simulations)

2.	Create a synthetic simulation: Open and execute the notebook: `2_Create_Synthetic_simulation.ipynb`


3.	Run an iteration of BioFF with synthetic simulations: Open and execute the notebook: `3_BioFF_FUS_syntetic.ipynb`

4.	Run BioFF with "fresh" simulations: Open and execute the notebook: `4_BioFF_FUS(fresh-sim).ipynb`

# References

[[1]](https://link.springer.com/article/10.1140/epjb/s10051-021-00234-4) Köfinger, Jürgen, and Gerhard Hummer. "Empirical optimization of molecular simulation force fields by Bayesian inference." The European Physical Journal B 94.12 (2021): 1-12.

[[2]](https://pubs.acs.org/doi/full/10.1021/acs.jctc.0c01064) Benayad, Zakarya, et al. "Simulation of FUS protein condensates with an adapted coarse-grained model." Journal of Chemical Theory and Computation 17.1 (2020): 525-537.

